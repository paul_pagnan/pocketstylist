package au.com.kentandlime.pocketstylist.service.api;

import au.com.kentandlime.pocketstylist.BuildConfig;
import au.com.kentandlime.pocketstylist.model.AuthResponse;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import rx.Observable;

/**
 * Retrofit interface that handles authentication based API requests
 */
public interface AuthInterface {
    String SERVICE_ENDPOINT = BuildConfig.CP_API;

    @FormUrlEncoded
    @POST("/auth/local")
    Observable<AuthResponse> authenticateUser(
            @Field("email") String email,
            @Field("password") String password);
}
