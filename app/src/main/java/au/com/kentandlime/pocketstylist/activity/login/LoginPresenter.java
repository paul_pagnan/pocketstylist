package au.com.kentandlime.pocketstylist.activity.login;

import android.text.TextUtils;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.model.AuthResponse;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.ContentService;
import rx.Observable;
import rx.functions.Func1;

/**
 * The login presenter provides view logic to the fragment
 */
public class LoginPresenter {
    @Inject
    AuthService mAuthService;

    @Inject
    ContentService mContentService;

    /**
     * Instantiates the login presenter and injects dependencies
     */
    public LoginPresenter() {
        App.getInjectorComponent().inject(this);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public Observable<AuthResponse> attemptLogin(String email, String password) {
        return mAuthService.authenticateUser(email, password).flatMap(new Func1<AuthResponse,
                Observable<AuthResponse>>() {
            @Override
            public Observable<AuthResponse> call(final AuthResponse authResponse) {
                return mContentService.register()
                        .flatMap(new Func1<User, Observable<AuthResponse>>() {
                    @Override
                    public Observable<AuthResponse> call(User user) {
                        return Observable.just(authResponse);
                    }
                });
            }
        });
    }

    /**
     * Determines if the user has already logged in or not
     */
    public boolean isLoggedIn() {
        return mAuthService.isLoggedIn();
    }

    /**
     * Checks if the password is valid, ie longer than 4 characters
     * @param password
     * @return
     */
    public boolean isPasswordValid(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 4;
    }

    /**
     * Checks to see if the email is valid by ensuring it has an @ symbol in it
     * @param email
     * @return
     */
    public boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && email.contains("@");
    }

    /**
     * Determines if the user needs to be onboarded
     * @return
     */
    public boolean doesNeedOnboarding() {
        return mAuthService.doesNeedOnboarding();
    }
}
