package au.com.kentandlime.pocketstylist.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;

import au.com.kentandlime.pocketstylist.R;

/**
 * Provides helper functions for displaying dialogs
 */
public class DialogUtil {
    private Context mContext;

    public DialogUtil(Context context) {
        mContext = context;
    }

    /**
     * Shows a progress dialog with a title using string resource IDs
     * @param titleResId
     * @return
     */
    public ProgressDialog showProgressDialog(int titleResId) {
        return showProgressDialog(mContext.getString(titleResId));
    }

    /**
     * Shows a progress dialog with a title
     * @param title
     * @return
     */
    public ProgressDialog showProgressDialog(String title) {
        return ProgressDialog.show(mContext, title, mContext.getString(R.string.please_wait), true);
    }


    /**
     * Show a simple one button dismissible message dialog box and accepts string resource IDs as
     * it's params
     * @param title
     * @param message
     */
    public void showDismissibleDialog(int title, int message) {
        showDismissibleDialog(mContext.getString(title), mContext.getString(message));
    }

    /**
     * Show a simple one button dismissible message dialog box
     * @param title
     * @param message
     */
    public void showDismissibleDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * Shows a list dialog where the user can select a single option
     * @param title
     * @param items
     * @param callback
     */
    public void showOptionsDialog(String title, MaterialSimpleListItem[] items,
                                  MaterialSimpleListAdapter.Callback callback) {
        final MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter(callback);

        for (MaterialSimpleListItem item : items) {
            adapter.add(item);
        }

        new MaterialDialog.Builder(mContext)
                .title(title)
                .adapter(adapter, null)
                .show();
    }
}
