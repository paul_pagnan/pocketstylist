package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Service model to define the connection request to the socket io chat server
 */
public class SocketIoConnectionRequest {
    @SerializedName("email")
    private String mEmail;

    @SerializedName("stylist")
    private String mStylist;

    public SocketIoConnectionRequest(String email, String stylist) {
        mEmail = email;
        mStylist = stylist;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getStylist() {
        return mStylist;
    }

    public void setStylist(String stylist) {
        mStylist = stylist;
    }
}
