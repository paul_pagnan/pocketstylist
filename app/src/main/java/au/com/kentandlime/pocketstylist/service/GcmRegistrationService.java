package au.com.kentandlime.pocketstylist.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import rx.Subscriber;

/**
 * Service to handle GCM registration & token changes
 */
public class GcmRegistrationService extends IntentService {
    @Inject
    ContentService mContentService;

    @Inject
    AuthService mAuthService;

    private static final String TAG = "GCMRegistration";
    private static String sToken;
    private Handler mHandler = new Handler();

    /**
     * Creates a GCMInstanceIdListener service and passes the Tag for debugging.
     */
    public GcmRegistrationService() {
        super(TAG);
        App.getInjectorComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        refreshToken();
    }

    private String getToken() {
        if (sToken == null) refreshToken();
        return sToken;
    }

    /**
     * Refreshes the GCM token
     */
    private void refreshToken() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    InstanceID instanceID = InstanceID.getInstance(GcmRegistrationService.this);
                    sToken = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    sendRegistrationToServer();
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage());
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(null, null, null);
    }

    /**
     * Sends the token to the server so that it can be stored with the user's account
     */
    public void sendRegistrationToServer() {
        if (mAuthService.isLoggedIn()) {
            mContentService.registerGcmToken(getToken(), mAuthService.getUser()).subscribe(
                    new Subscriber<Void>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(Void aVoid) {
                }
            });
        }
    }
}
