package au.com.kentandlime.pocketstylist.activity.inspiration.lookbook;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.activity.BasePresenter;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.service.ContentService;

/**
 * Presenter to provide data to the Look Book Fragment view
 */
public class LookBookPresenter extends BasePresenter<List<Post>> {
    @Inject
    ContentService mContentService;

    private final LookBookFragment mView;
    private final LookBookType mLookBookType;

    /**
     * Instantiates the Inspiration presenter, injects dependencies and configures base presenter
     * @param view
     */
    public LookBookPresenter(LookBookFragment view, LookBookType type) {
        App.getInjectorComponent().inject(this);
        mView = view;
        mLookBookType = type;
        setDataProvider(getDataProvider());
    }

    /**
     * Loads the posts from the API and calls the callbacks on the view
     */
    private void getPosts() {
        startLoading();
        mContentService.getPosts(getCurrentPage(), mLookBookType).subscribe(getSubscriber(mView));
    }

    /**
     * Returns a new data provider that defines what happens when the listeners request more data
     * @return
     */
    private DataProvider getDataProvider() {
        return new DataProvider() {
            @Override
            public void loadMoreData() {
                getPosts();
            }
        };
    }

    /**
     * Defines what type of data this fragment will show, Look book data or Discover data
     */
    public enum LookBookType {
        LOOK_BOOK,
        DISCOVER
    }
}
