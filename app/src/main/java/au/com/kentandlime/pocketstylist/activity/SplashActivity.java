package au.com.kentandlime.pocketstylist.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import au.com.kentandlime.pocketstylist.R;

/**
 * The initial view of the app. Shows the splash screen with the K&L logo which animates away
 * when the app is ready.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        animateLogo();
    }

    /**
     * Animate the logo out of the way and fade in the login fragment
     */
    private void animateLogo() {
        final Animation translateAnim = AnimationUtils.loadAnimation(this, R.anim.anim_translate_up);
        final Animation fadeAnim = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
        translateAnim.reset();
        fadeAnim.reset();
        final ImageView image = (ImageView) findViewById(R.id.logo);
        final LinearLayout loginView = (LinearLayout) findViewById(R.id.login_fragment_view);
        loginView.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable()
        {
           @Override
           public void run()
           {
               loginView.setVisibility(View.VISIBLE);
               image.startAnimation(translateAnim);
               loginView.startAnimation(fadeAnim);
           }
        }, 1500);
    }
}