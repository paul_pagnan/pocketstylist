package au.com.kentandlime.pocketstylist.receiver;

import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.ChatService;
import au.com.kentandlime.pocketstylist.service.GcmNotificationListenerService;
import au.com.kentandlime.pocketstylist.util.NotificationUtil;

/**
 * Broadcast receiver to detect when a notification has been cancelled so we don't display
 * already read notifications again
 */
public class NotificationBroadcastReceiver extends WakefulBroadcastReceiver {
    @Inject
    ChatService mChatService;

    @Inject
    AuthService mAuthService;

    public NotificationBroadcastReceiver() {
        App.getInjectorComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(NotificationUtil.NOTIFICATION_DELETED_ACTION))
        {
            mChatService.seenAllMessages();
        } else if (action.equals(NotificationUtil.NOTIFICATION_REPLY_ACTION)) {
            replyAction(context, intent);
        }
    }

    /**
     * The user has pressed the reply button inline on the notification, now send it to the server
     * @param context
     * @param intent
     */
    private void replyAction(Context context, Intent intent) {
        final User user = mAuthService.getUser();
        final String message = getMessageText(intent);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();

        final ChatMessage chatMessage = new ChatMessage(message, ChatMessage.UserType.USER);
        chatMessage.save();

        NotificationUtil.showChatNotifications(context, mChatService, chatMessage, false);

        mChatService.connect(user);

        // Time consuming tasks get run on a new thread
        Runnable runnable = new Runnable() {
            public void run() {
                mChatService.sendMessage(message);
                mChatService.disconnect();
            }
        };
        new Handler().postDelayed(runnable, 2000);

    }

    /**
     * Gets the inputted text from the inline reply section of the message notification
     * @param intent
     * @return
     */
    private String getMessageText(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getString(NotificationUtil.KEY_TEXT_REPLY);
        }
        return null;
    }
}
