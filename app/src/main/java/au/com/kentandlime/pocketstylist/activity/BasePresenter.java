package au.com.kentandlime.pocketstylist.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import com.paginate.Paginate;

import au.com.kentandlime.pocketstylist.model.PaginatedResponse;
import rx.Subscriber;

/**
 * Base presenter that contains shared code for Presenters
 * Accepts a generic type which should be the type of data that this presenter deals with
 * e.g. List<Post>
 */
public class BasePresenter<T> {
    private boolean mLoadingInProgress;
    private boolean mHasLoadedAll;
    private int mCurrentPage;
    private boolean mIsRefreshing;
    private DataProvider mDataProvider;

    /**
     * Returns the generic template for the paginate callbacks
     * @return
     */
    public Paginate.Callbacks getPaginationCallbacks() {
        mCurrentPage = 0;
        mHasLoadedAll = false;
        mLoadingInProgress = false;
        return new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                if (!mIsRefreshing) {
                    mDataProvider.loadMoreData();
                    mCurrentPage++;
                }
            }

            @Override
            public boolean isLoading() {
                return mLoadingInProgress;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return mHasLoadedAll;
            }
        };
    }

    /**
     * Returns the pagination instance which is bound to the recycler view
     * @param recyclerView
     * @return
     */
    public Paginate getPaginater(RecyclerView recyclerView) {
        return Paginate.with(recyclerView, getPaginationCallbacks())
                .setLoadingTriggerThreshold(2)
                .build();
    }

    /**
     * Returns a generic subscriber which calls the RefreshablePaginateableContentView callbacks
     * on API lifecycle methods
     * @param view
     * @return
     */
    protected Subscriber<PaginatedResponse<T>> getSubscriber(
            final RefreshablePaginateableContentView view) {
        return new Subscriber<PaginatedResponse<T>>() {
            @Override
            public void onCompleted() {
                finishLoading();
                view.onComplete();
            }

            @Override
            public void onError(Throwable e) {
                if (view != null) view.onError();
                onCompleted();
            }

            @Override
            public void onNext(final PaginatedResponse<T> response) {
                setHasLoadedAll(response.isLastPage());

                if (shouldNotifyRefreshComplete()) {
                    view.finishRefreshing();
                }
                if (view != null) view.onItemsRetrieved(response.getData());
            }
        };
    }

    /**
     * Returns the SwipeRefreshLayout that is used to get more data when the user requests
     * a refresh by swiping down in the view
     * @return
     */
    public SwipeRefreshLayout.OnRefreshListener getRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mIsRefreshing = true;
                mCurrentPage = 0;
                mDataProvider.loadMoreData();
                mCurrentPage++;
            }
        };
    }

    /**
     * Starts loading the view
     */
    protected void startLoading() {
        mLoadingInProgress = true;
    }

    /**
     * Finished loading, update variables
     */
    protected void finishLoading() {
        mLoadingInProgress = false;
        mIsRefreshing = false;
    }

    /**
     * All posts have been loaded, update variable
     * @param hasLoadedAll
     */
    protected void setHasLoadedAll(boolean hasLoadedAll) {
        mHasLoadedAll = hasLoadedAll;
    }

    /**
     * Informs the presenter if it should notify the view that the refresh is complete when the
     * user activates the SwipeRefreshLayout
     * @return
     */
    protected boolean shouldNotifyRefreshComplete() {
        if (mIsRefreshing) {
            mIsRefreshing = false;
            return true;
        }
        return false;
    }

    /**
     * Returns the current page in the pagination cycle
     * @return
     */
    public int getCurrentPage() {
        return mCurrentPage;
    }

    /**
     * Sets the data provider which defines where to get the new data from
     * @param dataProvider
     */
    public void setDataProvider(DataProvider dataProvider) {
        mDataProvider = dataProvider;
    }

    /**
     * Defines the interface that must be provided, it specifies where to get the new data from
     * when paginating or refreshing
     */
    protected interface DataProvider {
        void loadMoreData();
    }
}
