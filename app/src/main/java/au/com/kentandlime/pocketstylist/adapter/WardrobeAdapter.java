package au.com.kentandlime.pocketstylist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Defines the gridview adapter for the wardrobe screen
 */
public class WardrobeAdapter extends RecyclerView.Adapter<WardrobeAdapter.ViewHolder> {
    private final Context mContext;
    private List<WardrobeItem> mWardrobeItems = new ArrayList<>();
    private static LayoutInflater sInflater = null;
    private OnClickListener mListener;

    public WardrobeAdapter(Context context) {
        sInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = sInflater.inflate(R.layout.list_item_look_thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final WardrobeItem wardrobeItem = mWardrobeItems.get(position);

        if (wardrobeItem != null) {
            Picasso.with(mContext).load(wardrobeItem.getThumbnailImageUrl())
                    .into(holder.mLookImageView);
        }

        holder.mLookImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(wardrobeItem);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mWardrobeItems == null) return 0;
        return mWardrobeItems.size();
    }

    /**
     * Appends a list of wardrobeItems to the end of the list
     * @param wardrobeItems
     */
    public void appendItems(List<WardrobeItem> wardrobeItems) {
        if (wardrobeItems != null) {
            mWardrobeItems.addAll(wardrobeItems);
        }
    }

    /**
     * Clears all items from the adapter
     */
    public void clear() {
        mWardrobeItems.clear();
    }


    /**
     * Sets the on click listener which is called when a wardrobe item is pressed
     */
    public void setOnClickListener(OnClickListener listener) {
        mListener = listener;
    }
    /**
     * Removes a specific wardrobe item from the list, used for when a wardrobe item is deleted
     * @param wardrobeItem
     */
    public void remove(WardrobeItem wardrobeItem) {
        Iterator<WardrobeItem> iterator = mWardrobeItems.iterator();
        while(iterator.hasNext())
        {
            WardrobeItem nextPost = iterator.next();
            if (nextPost.getId().equals(wardrobeItem.getId())) {
                iterator.remove();
            }
        }
    }

    /**
     * Adds a wardrobe item to the start of the list, used for when a new item has been uploaded
     * by the user
     * @param wardrobeItem
     */
    public void prependItem(WardrobeItem wardrobeItem) {
        mWardrobeItems.add(0, wardrobeItem);
    }

    /**
     * The view holder for each image thumbnail
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.look_thumbnail_image)
        ImageView mLookImageView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    /**
     * Interface to notify the view when a wardrobe item has been clicked
     */
    public interface OnClickListener {
        void onClick(WardrobeItem wardrobeItem);
    }
}
