package au.com.kentandlime.pocketstylist.service.api;

import java.util.List;

import au.com.kentandlime.pocketstylist.BuildConfig;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.Comment;
import au.com.kentandlime.pocketstylist.model.PaginatedResponse;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import rx.Observable;

/**
 * Retrofit interface that communicates with the content API
 */
public interface ContentApiInterface {
    String SERVICE_ENDPOINT = BuildConfig.CONTENT_API;

    @GET("/register")
    Observable<User> register();

    @GET("/posts/lookBook/{page}")
    Observable<PaginatedResponse<List<Post>>> getLookBookPosts(@Path("page") int page);

    @GET("/posts/discover/{page}")
    Observable<PaginatedResponse<List<Post>>> getDiscoverPosts(@Path("page") int page);

    @GET("/posts/myLooks/{page}")
    Observable<PaginatedResponse<List<Post>>> getMyLooks(@Path("page") int page);

    @POST("/posts/{id}/like")
    Observable<Void> likePost(@Path("id") String id);

    @DELETE("/posts/{id}/like")
    Observable<Void> unlikePost(@Path("id") String id);

    @GET("/posts/{id}/comments")
    Observable<List<Comment>> getComments(@Path("id") String id);

    @FormUrlEncoded
    @POST("/posts/{id}/comments")
    Observable<Void> postComment(@Path("id") String id, @Field("comment") String comment);

    @GET("/messages/{page}")
    Observable<PaginatedResponse<List<ChatMessage>>> getMessages(@Path("page") int page);

    @FormUrlEncoded
    @POST("/user/{id}/registerGcm")
    Observable<Void> registerGcmToken(@Path("id") String userId, @Field("token") String token);

    @FormUrlEncoded
    @POST("/wardrobe")
    Observable<WardrobeItem> addWardrobeItem(@Field("wardrobeItem") String wardrobeItem);

    @GET("/wardrobe/{page}")
    Observable<PaginatedResponse<List<WardrobeItem>>> getWardrobeItems(@Path("page") int page);

    @FormUrlEncoded
    @PUT("/wardrobe/:id")
    Observable<Void> updateWardrobeItem(@Path("id") String id,
                                        @Field("wardrobeItem") String wardrobeItem);

    @DELETE("/wardrobe/{id}")
    Observable<Void> deleteWardrobeItem(@Path("id") String id);
}