package au.com.kentandlime.pocketstylist.activity.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.MainActivity;
import au.com.kentandlime.pocketstylist.activity.OnboardingActivity;
import au.com.kentandlime.pocketstylist.model.AuthResponse;
import au.com.kentandlime.pocketstylist.util.DialogUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observer;

/**
 * A login fragment that offers login via email/password. Appears within the SplashActivity
 */
public class LoginFragment extends Fragment {
    @Inject
    LoginPresenter mLoginPresenter;

    @BindView(R.id.email_login_et)
    EditText mEmailView;

    @BindView(R.id.password_login_et)
    EditText mPasswordView;

    private DialogUtil mDialogUtil;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        App.getInjectorComponent().inject(this);

        mDialogUtil = new DialogUtil(getContext());

        // Is the user logged in, if so, skip this screen
        if (mLoginPresenter.isLoggedIn()) gotoMainActivity();

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    login();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) getActivity().findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        setActionListeners();
    }

    /**
     * Navigates to the next activity
     */
    private void gotoMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }

    /**
     * Navigates to the onboarding activity
     */
    private void gotoOnBoarding() {
        Intent intent = new Intent(getActivity(), OnboardingActivity.class);
        startActivity(intent);
    }

    /**
     * Set the action listeners for the signup and forgot password links.
     * Opens in the default web browser
     */
    private void setActionListeners() {
        TextView forgotPass = (TextView) getActivity().findViewById(R.id.txt_forgot_password);
        TextView signUp = (TextView) getActivity().findViewById(R.id.txt_sign_up);
        forgotPass.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.forgot_password_url)));
                startActivity(intent);
            }
        });
        signUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.sign_up_url)));
                startActivity(intent);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    /**
     * Check if the login form is valid by calling the presenter
     * @param email
     * @param password
     * @return
     */
    private boolean isFormValid(String email, String password) {
        if (!mLoginPresenter.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
            return false;
        } else if (!mLoginPresenter.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            mPasswordView.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * Actually do the login by calling the Auth service
     */
    private void login() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        if (isFormValid(email, password)) {
            final ProgressDialog dialog = mDialogUtil.showProgressDialog(
                    getString(R.string.logging_in));

            mLoginPresenter.attemptLogin(
                mEmailView.getText().toString(),
                mPasswordView.getText().toString()).subscribe(new Observer<AuthResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    dialog.dismiss();
                    mDialogUtil.showDismissibleDialog(
                            getString(R.string.login_error),
                            getString(R.string.error_incorrect_login));
                }

                @Override
                public void onNext(AuthResponse apiResponse) {
                    dialog.dismiss();
                    if (mLoginPresenter.doesNeedOnboarding()) {
                        gotoOnBoarding();
                    } else {
                        gotoMainActivity();
                    }
                    getActivity().finish();
                }
            });
        }
    }
}

