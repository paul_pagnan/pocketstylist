package au.com.kentandlime.pocketstylist.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Defines the app module to provide singleton context to dagger dependencies
 */
@Module
public class AppModule {
    Application mApplication;
    Context mContext;

    public AppModule(Application application) {
        mApplication = application;
        mContext = application.getApplicationContext();
    }

    public Context getContext() {
        return mContext;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return mContext;
    }
}
