package au.com.kentandlime.pocketstylist.activity.chat;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.activity.BasePresenter;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.ChatService;
import au.com.kentandlime.pocketstylist.service.ContentService;

/**
 * Provides view logic and service integration for the chat fragment
 */
public class ChatPresenter extends BasePresenter<List<ChatMessage>> {
    @Inject
    ContentService mContentService;

    @Inject
    AuthService mAuthService;

    @Inject
    ChatService mChatService;

    private ChatFragment mView;

    /**
     * Instantiates the chat presenter, injects dependencies and initialises the SocketIO Connection
     * @param view
     */
    public ChatPresenter(ChatFragment view) {
        App.getInjectorComponent().inject(this);
        mView = view;
        setDataProvider(getDataProvider());
        initSocketIoConnection();
    }

    /**
     * Gets the message history from the server asynchronously by page
     */
    private void loadMessages() {
        mContentService.getMessages(getCurrentPage()).subscribe(getSubscriber(mView));
    }

    public void sendMessage(String chatMessage) {
        mChatService.sendMessage(chatMessage);
    }

    /**
     * Connects to the socket io server
     */
    private void initSocketIoConnection() {
        ChatService.OnMessageReceivedListener listener =
                new ChatService.OnMessageReceivedListener() {
            @Override
            public void messageReceived(ChatMessage message) {
                if (mView != null) mView.onMessageLoaded(message);
            }
        };
        mChatService.setOnMessageRecievedListener(listener);
        connect();
        mChatService.seenAllMessages();
    }

    /**
     * Connects to the socket io server
     */
    public void connect() {
        if (!mChatService.isConnected()) {
            mChatService.connect(mAuthService.getUser());
        }
    }

    /**
     * Calls the chat service to disconnect from the socket io server
     */
    public void disconnect() {
        mChatService.disconnect();
    }

    /**
     * Returns a new data provider that defines what happens when the listeners request more data
     * @return
     */
    private DataProvider getDataProvider() {
        return new DataProvider() {
            @Override
            public void loadMoreData() {
                loadMessages();
            }
        };
    }

    /**
     * Gets the current user
     * @return
     */
    public User getUser() {
        return mAuthService.getUser();
    }

    /**
     * Gets the unread posts by the user
     * @return
     */
    public List<ChatMessage> getUnreadPosts() {
        return mChatService.getAllUnreadMessages();
    }
}
