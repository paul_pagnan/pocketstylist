package au.com.kentandlime.pocketstylist.dagger;

import javax.inject.Singleton;

import au.com.kentandlime.pocketstylist.activity.MainActivity;
import au.com.kentandlime.pocketstylist.activity.OnboardingActivity;
import au.com.kentandlime.pocketstylist.activity.chat.ChatPresenter;
import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationPresenter;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.CommentDialogFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookPresenter;
import au.com.kentandlime.pocketstylist.activity.inspiration.mylooks.MyLooksFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.mylooks.MyLooksPresenter;
import au.com.kentandlime.pocketstylist.activity.login.LoginFragment;
import au.com.kentandlime.pocketstylist.activity.login.LoginPresenter;
import au.com.kentandlime.pocketstylist.activity.wardrobe.ViewWardrobeActivity;
import au.com.kentandlime.pocketstylist.activity.wardrobe.WardrobeFragment;
import au.com.kentandlime.pocketstylist.activity.wardrobe.WardrobePresenter;
import au.com.kentandlime.pocketstylist.receiver.NotificationBroadcastReceiver;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.GcmNotificationListenerService;
import au.com.kentandlime.pocketstylist.service.GcmRegistrationService;
import au.com.kentandlime.pocketstylist.service.WardrobeUploadService;
import dagger.Component;

/**
 * Defines the injectable components in the app
 */
@Singleton
@Component(modules={AppModule.class, InjectorModule.class})
public interface InjectorComponent {
    void inject(MainActivity activity);
    void inject(OnboardingActivity activity);
    void inject(ViewWardrobeActivity activity);

    void inject(InspirationFragment fragment);
    void inject(LoginFragment fragment);
    void inject(LookBookFragment fragment);
    void inject(MyLooksFragment fragment);
    void inject(CommentDialogFragment fragment);
    void inject(WardrobeFragment fragment);

    void inject(LoginPresenter presenter);
    void inject(InspirationPresenter presenter);
    void inject(ChatPresenter presenter);
    void inject(LookBookPresenter presenter);
    void inject(MyLooksPresenter presenter);
    void inject(WardrobePresenter presenter);

    void inject(AuthService service);
    void inject(GcmRegistrationService service);
    void inject(GcmNotificationListenerService service);
    void inject(WardrobeUploadService service);
    
    void inject(NotificationBroadcastReceiver receiver);
}
