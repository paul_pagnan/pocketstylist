package au.com.kentandlime.pocketstylist.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.chat.ChatFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationFragment;
import au.com.kentandlime.pocketstylist.activity.wardrobe.WardrobeFragment;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.GcmRegistrationService;
import au.com.kentandlime.pocketstylist.util.DialogUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The main activity. Primarily only handles the logic for the Navigation Drawer. The sub fragments
 * actually do the main functionality
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String KEY_GOTO_CHAT = "goto_chat";
    public static final String KEY_GOTO_WARDROBE = "goto_wardrobe";

    @Inject
    AuthService mAuthService;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.getInjectorComponent().inject(this);
        ButterKnife.bind(this);
        DialogUtil dialogUtil = new DialogUtil(this);

        replaceFragment(new InspirationFragment());

        mToolbar.setTitleTextAppearance(this, R.style.ThinWhite);
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        populateUserDetailsInNav();

        // Now that we are logged in we can associate the Gcm token with the user account
        Intent intent = new Intent(this, GcmRegistrationService.class);
        startService(intent);

        // User is coming from the notification, go straight to the chat view
        if (getIntent().getBooleanExtra(KEY_GOTO_CHAT, false)) {
            replaceFragment(new ChatFragment());
        } else if (getIntent().getBooleanExtra(KEY_GOTO_WARDROBE, false)) {
            replaceFragment(new WardrobeFragment());

        }
    }

    /**
     * Populates the user details text boxes in the nav drawer
     */
    private void populateUserDetailsInNav() {
        View navHeader = mNavigationView.getHeaderView(0);
        final TextView nameText = (TextView) navHeader.findViewById(R.id.nav_header_name_txt);
        final TextView emailText = (TextView) navHeader.findViewById(R.id.nav_header_email_txt);

        if (nameText != null && emailText != null) {
            User user = mAuthService.getUser();
            nameText.setText(user.getFirstName());
            emailText.setText(user.getEmail());
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment;
        int id = item.getItemId();
        switch(id) {
            default:
            case R.id.nav_look_book:
                fragment = new InspirationFragment();
                break;
            case R.id.nav_chat:
                fragment = new ChatFragment();
                break;
            case R.id.nav_wardrobe:
                fragment = new WardrobeFragment();
                break;
        }

        replaceFragment(fragment);

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Replaces the fragment inside the main fragment layout
     * @param fragment
     */
    private void replaceFragment(Fragment fragment) {
        // Hide the virtual keyboard
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        // Reset scroll behaviour
        AppBarLayout.LayoutParams appBarParams =
                (AppBarLayout.LayoutParams) mToolbar.getLayoutParams();
        appBarParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED);
        mToolbar.setLayoutParams(appBarParams);

        // Replace the fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commitNow();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
