package au.com.kentandlime.pocketstylist.activity.chat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;
import com.squareup.picasso.Picasso;

import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.RefreshablePaginateableContentView;
import au.com.kentandlime.pocketstylist.adapter.ChatAdapter;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.GcmNotificationListenerService;
import au.com.kentandlime.pocketstylist.util.NotificationUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Allows the user to instant chat to their stylist
 */
public class ChatFragment extends Fragment
        implements RefreshablePaginateableContentView<List<ChatMessage>>{
    @BindView(R.id.chat_edit_text)
    EditText mChatEt;

    @BindView(R.id.chat_recycler_view)
    RecyclerView mChatRecycler;

    @BindView(R.id.chat_send_btn)
    ImageView mSendBtn;

    @BindView(R.id.chat_header_stylist_image_view)
    ImageView mStylistImage;

    @BindView(R.id.chat_header_stylist_name_txt)
    TextView mStylistGreeting;

    @BindView(R.id.chat_stylist_greeting)
    LinearLayout mStylistGreetingContainer;

    @BindView(R.id.chat_empty_list)
    TextView mEmptyView;

    private ChatAdapter mChatAdapter;
    private Paginate mPagination;
    private ChatPresenter mChatPresenter;
    private boolean mShouldExecuteOnResume;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.stylist_chat));

        // User has navigated to the chat. Get rid of the notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat
                .from(getContext());
        notificationManager.cancel(NotificationUtil.NOTIFICATION_ID);
    }

    @Override
    public void onStop() {
        super.onStop();
        mChatPresenter.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Ignore the first time it is run on activity start.
        // We only care about subsequent onResumes

        if (!mShouldExecuteOnResume) {
            mShouldExecuteOnResume = true;
            return;
        }

        List<ChatMessage> unreadPosts = mChatPresenter.getUnreadPosts();
        mChatAdapter.prependMessages(unreadPosts);
        notifyViewUpdate();
    }

    @Override
    public void onStart() {
        super.onStart();
        mChatPresenter.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mChatPresenter = new ChatPresenter(this);

        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, layout);

        mChatAdapter = new ChatAdapter(this.getContext(), mChatPresenter.getUser());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);

        mChatRecycler.setLayoutManager(layoutManager);
        mChatRecycler.setAdapter(mChatAdapter);

        // Setup pagination
        mPagination = mChatPresenter.getPaginater(mChatRecycler);

        // Setup interactions
        setupChatSenderEditText();
        setupChatSendListener();

        return layout;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChatPresenter.disconnect();
    }

    /**
     * Defines  the logic that modifies the view based on what is typed into the chat input box
     */
    private void setupChatSenderEditText() {
        mChatEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    mSendBtn.setImageDrawable(getResources().getDrawable(R.drawable.send_disabled));
                    mSendBtn.setClickable(false);
                } else {
                    mSendBtn.setImageDrawable(getResources().getDrawable(R.drawable.send));
                    mSendBtn.setClickable(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * Defines the logic that actually sends the message when the user clicks the send button
     */
    private void setupChatSendListener() {
        mSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chatMessage = mChatEt.getText().toString();
                if (!chatMessage.isEmpty()) {
                    mChatPresenter.sendMessage(chatMessage);
                    mChatEt.setText(null);

                    mChatAdapter.addMessage(new ChatMessage(chatMessage,
                            ChatMessage.UserType.USER));
                    notifyViewUpdate();
                }
            }
        });
    }

    /**
     * Hides the stylist greeting at the start of message list
     */
    private void hideStylistGreeting() {
        if (mStylistGreeting.getVisibility() == View.VISIBLE) {
            mStylistGreetingContainer.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.GONE);
        }
    }


    /**
     * Called when a single message is received from the socket io server
     * @param message
     */
    public void onMessageLoaded(final ChatMessage message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChatAdapter.addMessage(message);
                notifyViewUpdate();
            }
        });
    }

    /**
     * If it is the last message in the list, show the stylist header
     */
    public void showStylistGreeting() {
        User user = mChatPresenter.getUser();

        String greeting = getString(R.string.stylist_greeting)
                .concat(" ").concat(user.getStylist());
        mStylistGreeting.setText(greeting);
        Picasso.with(getContext()).load(user.getStylistImageUrl()).into(mStylistImage);

        mStylistGreetingContainer.setVisibility(View.VISIBLE);

        mPagination.unbind();
    }

    /**
     * Notifies tha adapter that the list has changed and hides the stylist greeting
     */
    private void notifyViewUpdate() {
        mChatAdapter.notifyDataSetChanged();
        if (mChatAdapter.getItemCount() > 0) {
            hideStylistGreeting();
        }
    }

    @Override
    public void onComplete() {
        if (mChatPresenter.getCurrentPage() == 0 && mChatAdapter.getItemCount() == 0) {
            showStylistGreeting();
        }
    }

    @Override
    public void onError() {
        mPagination.unbind();
        Toast.makeText(getContext(), getString(R.string.error_load_messages),
                Toast.LENGTH_SHORT).show();

        if (mChatAdapter.getItemCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishRefreshing() {
        // fragment does not support refreshing
    }

    @Override
    public void onItemsRetrieved(List<ChatMessage> data) {
        mChatAdapter.appendMessages(data);
        notifyViewUpdate();
    }
}
