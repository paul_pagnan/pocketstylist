package au.com.kentandlime.pocketstylist.activity.inspiration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.SplashActivity;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookPresenter;
import au.com.kentandlime.pocketstylist.activity.inspiration.mylooks.MyLooksFragment;
import au.com.kentandlime.pocketstylist.adapter.ViewPagerAdapter;
import au.com.kentandlime.pocketstylist.dependency.SlidingTabLayout;
import au.com.kentandlime.pocketstylist.service.AuthService;

/**
 * The container for the sliding tab layout.
 */
public class InspirationFragment extends Fragment {
    @Inject
    AuthService mAuthService;

    private InspirationPresenter mPresenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        App.getInjectorComponent().inject(this);

        mPresenter = new InspirationPresenter(getContext());

        ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        SlidingTabLayout tabLayout = (SlidingTabLayout) getActivity().findViewById(R.id.tabs);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.kalBeige);
            }
        });
        viewPager.setCurrentItem(1);
        tabLayout.setViewPager(viewPager);

        setHasOptionsMenu(true);
    }

    /**
     * Set up the view pager used by the sliding tab layout
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        LookBookFragment lookBookFragment = new LookBookFragment();
        LookBookFragment discoverFragment = new LookBookFragment();

        lookBookFragment.setType(LookBookPresenter.LookBookType.LOOK_BOOK);
        discoverFragment.setType(LookBookPresenter.LookBookType.DISCOVER);


        adapter.addFragment(getString(R.string.my_looks), new MyLooksFragment());
        adapter.addFragment(getString(R.string.look_book), lookBookFragment);
        adapter.addFragment(getString(R.string.discover), discoverFragment);
        // Prevent viewpager from destroying the views when they are off-screen
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inspiration, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_inspiration_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.log_out) {
            mAuthService.logOut();
            Intent intent = new Intent(getActivity(), SplashActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
        return false;
    }
}
