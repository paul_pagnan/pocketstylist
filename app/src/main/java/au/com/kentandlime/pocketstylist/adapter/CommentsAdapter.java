package au.com.kentandlime.pocketstylist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.Comment;

/**
 * Defines the adapter to display the comments list on a post
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    private List<Comment> mComments;
    private static LayoutInflater sInflater = null;

    public CommentsAdapter(Context context) {
        sInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mComments = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = sInflater.inflate(R.layout.list_item_comment, parent, false);
        return new ViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comment comment = mComments.get(position);

        PrettyTime prettyTime = new PrettyTime();
        DateTime localDate = LocalDateTime.fromDateFields(comment.getCreatedAt())
                .toDateTime(DateTimeZone.UTC);
        holder.timeTxt.setText(prettyTime.format(localDate.toDate()));

        holder.nameTxt.setText(comment.getUser().getFirstName());
        holder.commentTxt.setText(comment.getComment());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    /**
     * Append a list of comments to the end of the list
     * @param comments
     */
    public void appendAll(List<Comment> comments) {
        mComments.addAll(comments);
    }

    /**
     * Adds a comment to the end of the list
     */
    public void append(Comment comment) {
        mComments.add(comment);
    }

    /**
     * The view holder for each comment item
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView timeTxt;
        TextView commentTxt;
        TextView nameTxt;

        public ViewHolder(View v) {
            super(v);
            commentTxt = (TextView) v.findViewById(R.id.comment_txt);
            nameTxt = (TextView) v.findViewById(R.id.comment_name_txt);
            timeTxt = (TextView) v.findViewById(R.id.comment_time_txt);
        }
    }
}
