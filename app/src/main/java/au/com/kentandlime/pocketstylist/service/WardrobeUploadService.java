package au.com.kentandlime.pocketstylist.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Action;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.BuildConfig;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.MainActivity;
import au.com.kentandlime.pocketstylist.activity.wardrobe.AddWardrobeActivity;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import rx.Subscriber;

/**
 * Service to upload an image to s3
 */
public class WardrobeUploadService extends IntentService {
    @Inject
    ContentService mContentService;
    public static final String WARDROBE_UPDATE = "WardrobeUpdate";
    public static final String KEY_WARDROBE = "WardrobeItem";
    public static final String FILE_EXTENSION = ".jpg";
    private static final int COMPRESSION_QUALITY = 60;
    private static final int RERTY_INTENT_REQUEST_CODE = 3;
    private static final int INTENT_REQUEST_CODE = 2;
    private static final int NOTIFICATION_ID = 2;

    private static final int SCALE_HEIGHT = 250;
    private static final int RES_FULL_SIZE = 0;
    private static final int RES_THUMB = 1;
    private static final String THUMB_FILE_SUFFIX = "-thumbnail";
    private static final String SERVICE_NAME = "WordrobeUploadService";
    private static final String S3_ACCESS_KEY_ID = BuildConfig.S3_ACCESS_KEY_ID;
    private static final String S3_SECRET_KEY = BuildConfig.S3_SECRET_KEY;
    private static final String S3_BUCKET = BuildConfig.S3_BUCKET;
    private static final String S3_URL = BuildConfig.S3_URL;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private Intent mIntent;

    public WardrobeUploadService() {
        super(SERVICE_NAME);
        App.getInjectorComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        showProgressNotification();
        mIntent = intent;

        Uri imageUri = intent.getParcelableExtra(AddWardrobeActivity.KEY_WARDROBE_IMAGE);
        boolean isContentUri = intent.getBooleanExtra(AddWardrobeActivity.KEY_IS_CONTENT_URI,
                false);
        WardrobeItem wardrobeItem = intent.getParcelableExtra(
                AddWardrobeActivity.KEY_WARDROBE_ITEM);

        AmazonS3 s3 = new AmazonS3Client(getCredentials());
        TransferUtility transferUtility = new TransferUtility(s3, this);

        try {
            File fullRes = getImageFile(RES_FULL_SIZE, imageUri, isContentUri);
            File thumbnail = getImageFile(RES_THUMB, imageUri, isContentUri);

            transferUtility.upload(S3_BUCKET, fullRes.getName(), fullRes);
            transferUtility.upload(S3_BUCKET, thumbnail.getName(), thumbnail);

            sendToServer(fullRes, thumbnail, wardrobeItem);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showError();
        }
    }

    private File getImageFile(int size, Uri imageUri, boolean isContentUri)
            throws FileNotFoundException {
        String suffix = size == RES_FULL_SIZE ? "" : THUMB_FILE_SUFFIX;
        String fileName = UUID.randomUUID().toString();
        File file = createTempFile(fileName, suffix);
        Bitmap bmp = getBitmap(imageUri, isContentUri);

        FileOutputStream bos = new FileOutputStream(file);

        if (size == RES_THUMB) {
            bmp = getThumbnailImage(bmp);
        }

        bmp.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY, bos);

        return file;
    }

    /**
     * Returns a scaled down version of the full size image
     * @param bitmap
     * @return
     */
    public Bitmap getThumbnailImage(Bitmap bitmap) {
        Bitmap resizedBitmap;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;
        if(originalHeight > originalWidth) {
            newHeight = SCALE_HEIGHT;
            multFactor = (float) originalWidth/(float) originalHeight;
            newWidth = (int) (newHeight*multFactor);
        } else if(originalWidth > originalHeight) {
            newWidth = SCALE_HEIGHT;
            multFactor = (float) originalHeight/ (float)originalWidth;
            newHeight = (int) (newWidth*multFactor);
        } else if(originalHeight == originalWidth) {
            newHeight = SCALE_HEIGHT;
            newWidth = SCALE_HEIGHT;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }

    /**
     * Sends the wardrobe data to the server
     * @param fullRes
     * @param thumbnail
     * @param wardrobeItem
     */
    private void sendToServer(File fullRes, File thumbnail, final WardrobeItem wardrobeItem) {
        wardrobeItem.setImageUrl(getImageUrl(fullRes.getName()));
        wardrobeItem.setThumbnailImageUrl(getImageUrl(thumbnail.getName()));
        mContentService.addWardrobeItem(wardrobeItem).subscribe(new Subscriber<WardrobeItem>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                showError();
            }

            @Override
            public void onNext(WardrobeItem returnedWardrobeItem) {
                showSuccess();

                // Notify the WardrobeFragment that there is a new wardrobe item to be displayed
                Intent notifyIntent = new Intent().setAction(WARDROBE_UPDATE);
                notifyIntent.putExtra(KEY_WARDROBE, returnedWardrobeItem);
                sendBroadcast(notifyIntent);
            }
        });
    }

    /**
     * Creates a temp file for the content URI file to be stored
     * @return
     * @param fileName
     * @param suffix
     */
    private File createTempFile(String fileName, String suffix) {
        try {
            fileName = fileName + suffix;
            File directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(fileName, FILE_EXTENSION, directory);
            return new File(imageFile.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Resolves a bitmap file from a URI, accepts either a file uri or a content uri
     * @param imageUri
     * @param isContentUri
     * @return
     * @throws FileNotFoundException
     */
    private Bitmap getBitmap(Uri imageUri, boolean isContentUri) throws FileNotFoundException {
        if (isContentUri) return getImageFromContentUri(imageUri);
        return BitmapFactory.decodeFile(imageUri.getPath());
    }

    private void showProgressNotification() {
        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(getString(R.string.adding_to_wardrobe))
                .setContentText(getString(R.string.upload_in_progress))
                .setOngoing(true)
                .setSmallIcon(R.mipmap.notification_icon);
        mBuilder.setProgress(0, 0, true);
        mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /**
     * Shows an upload error in notifications and provide a retry button
     */
    private void showError() {
        String retryText = getString(R.string.retry);
        Intent retryIntent = mIntent;

        PendingIntent retryPendingIntent = PendingIntent.getService(this, RERTY_INTENT_REQUEST_CODE,
                retryIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Action action = new Action.Builder(
                R.drawable.ic_refresh_black_24dp, retryText, retryPendingIntent)
                .build();
        showOutcomeNotification(R.string.could_not_add_to_wardrobe, R.string.upload_error, action);
    }


    /**
     * Shows a success notification when the upload has finished
     */
    private void showSuccess() {
        showOutcomeNotification(R.string.upload_finished, R.string.wardrobe_added_successfully, null);
    }

    /**
     * Shows a notification
     * @param titleResId
     * @param contentResId
     */
    private void showOutcomeNotification(int titleResId, int contentResId,
                                         Action action) {
        mBuilder = new NotificationCompat.Builder(this);
        String content = getString(contentResId);

        Intent contentIntent = new Intent(this, MainActivity.class);
        contentIntent.putExtra(MainActivity.KEY_GOTO_WARDROBE, true);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, INTENT_REQUEST_CODE,
                contentIntent, 0);

        mBuilder.setContentTitle(getString(titleResId))
                .setContentText(content)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(content))
                .setAutoCancel(true)
                .setContentIntent(contentPendingIntent)
                .setColor(getResources().getColor(R.color.kalBeige))
                .setSmallIcon(R.mipmap.notification_icon);

        if (action != null) {
           mBuilder.addAction(action)
                   .setDefaults(Notification.DEFAULT_VIBRATE
                           | Notification.DEFAULT_SOUND
                           | Notification.FLAG_SHOW_LIGHTS)
                   .setPriority(Notification.PRIORITY_MAX);
        }

        mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /**
     * Returns the secret AWS credentials
     * @return
     */
    private AWSCredentials getCredentials() {
        return new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return S3_ACCESS_KEY_ID;
            }

            @Override
            public String getAWSSecretKey() {
                return S3_SECRET_KEY;
            }
        };
    }

    /**
     * Resolves an image from content URI
     * @param contentUri
     * @return
     * @throws FileNotFoundException
     */
    private Bitmap getImageFromContentUri(Uri contentUri) throws FileNotFoundException {
        ContentResolver cr = getContentResolver();
        InputStream in = cr.openInputStream(contentUri);
        return BitmapFactory.decodeStream(in);
    }

    /**
     * Resolves the publicly accessible image URL from the S3 server
     * @param fileName
     * @return
     */
    private String getImageUrl(String fileName) {
        return S3_URL.concat("/").concat(S3_BUCKET).concat("/").concat(fileName);
    }
}
