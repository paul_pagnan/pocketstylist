package au.com.kentandlime.pocketstylist.activity.wardrobe;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.BasePresenter;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import au.com.kentandlime.pocketstylist.service.ContentService;
import au.com.kentandlime.pocketstylist.util.DialogUtil;

/**
 * Presenter to provide the wardrobe fragment with data
 */
public class WardrobePresenter extends BasePresenter<List<WardrobeItem>> {
    @Inject
    ContentService mContentService;

    private final DialogUtil mDialogUtil;
    private final WardrobeFragment mView;
    private final Context mContext;

    /**
     * Instantiates the presenter and configures the base presenter
     * @param view
     * @param context
     */
    public WardrobePresenter(WardrobeFragment view, Context context) {
        App.getInjectorComponent().inject(this);

        mView = view;
        mDialogUtil = new DialogUtil(context);
        mContext = context;
        setDataProvider(getDataProvider());
    }

    /**
     * Gets the wardrobe items from the service
     */
    private void getWardrobeItems() {
        startLoading();
        mContentService.getWardrobeItems(getCurrentPage()).subscribe(getSubscriber(mView));
    }

    /**
     * Shows a dialog for the user to choose how they want to upload an image
     */
    public void showImageOptionsDialog(final ImageProvider imageProvider) {
        MaterialSimpleListItem[] listItems = new MaterialSimpleListItem[] {
                new MaterialSimpleListItem.Builder(mContext)
                        .id(WardrobeFragment.KEY_TAKE_PHOTO)
                        .content(R.string.take_a_photo)
                        .icon(R.drawable.ic_camera_alt_black_24dp)
                        .backgroundColorRes(R.color.white)
                        .build(),
                new MaterialSimpleListItem.Builder(mContext)
                        .id(WardrobeFragment.KEY_CHOOSE_FROM_GALLERY)
                        .content(R.string.choose_from_gallery)
                        .icon(R.drawable.ic_collections_black_24dp)
                        .backgroundColorRes(R.color.white)
                        .build(),
        };

        MaterialSimpleListAdapter.Callback callback = new MaterialSimpleListAdapter.Callback() {
            @Override
            public void onMaterialListItemSelected(MaterialDialog dialog, int index,
                                                   MaterialSimpleListItem item) {
                dialog.hide();
                switch((int) item.getId()) {
                    case WardrobeFragment.KEY_TAKE_PHOTO:
                        imageProvider.fromCamera();
                        break;
                    case WardrobeFragment.KEY_CHOOSE_FROM_GALLERY:
                        imageProvider.fromGallery();
                        break;
                }
            }
        };

        mDialogUtil.showOptionsDialog(mContext.getString(R.string.add_to_wardrobe),
                listItems, callback);
    }

    /**
     * Defines where to get the new data once the paginater or refresher requests it
     * @return
     */
    public DataProvider getDataProvider() {
        return new DataProvider() {
            @Override
            public void loadMoreData() {
                getWardrobeItems();
            }
        };
    }

    /**
     * Defines the methods for how to retrieve an image, can be either from the gallery or the
     * camera
     */
    public interface ImageProvider {
        void fromCamera();
        void fromGallery();
    }
}
