package au.com.kentandlime.pocketstylist.activity.inspiration.mylooks;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.RefreshablePaginateableContentView;
import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationPresenter;
import au.com.kentandlime.pocketstylist.adapter.ItemOffsetDecoration;
import au.com.kentandlime.pocketstylist.adapter.MyLooksAdapter;
import au.com.kentandlime.pocketstylist.model.Post;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Displays a list of posts that match the customers style preferences
 */
public class MyLooksFragment extends Fragment
        implements RefreshablePaginateableContentView<List<Post>> {
    @Inject
    InspirationPresenter mInspirationPresenter;

    private MyLooksAdapter mMyLooksAdapter;

    @BindView(R.id.looks_empty_view)
    TextView mNoPostsTxt;

    @BindView(R.id.looks_recycler_view)
    RecyclerView mLooksRecycler;

    @BindView(R.id.look_refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    private Paginate mPagination;
    private MyLooksPresenter mMyLooksPresenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.look_book));

        AppBarLayout.LayoutParams appBarParams =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        appBarParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL |
                AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        toolbar.setLayoutParams(appBarParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_look_book, container, false);
        App.getInjectorComponent().inject(this);
        ButterKnife.bind(this, layout);
        mMyLooksPresenter = new MyLooksPresenter(this);
        mInspirationPresenter.attachMyLooksFragment(this);

        mNoPostsTxt.setText(getString(R.string.no_posts_go_like));

        mMyLooksAdapter = new MyLooksAdapter(getContext(), mLooksRecycler);

        mLooksRecycler.setLayoutManager(new GridLayoutManager(getActivity(),
                ItemOffsetDecoration.NUM_COLUMNS));
        mLooksRecycler.setAdapter(mMyLooksAdapter);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(
                getContext(),
                R.dimen.item_offset,
                ItemOffsetDecoration.NUM_COLUMNS);
        mLooksRecycler.addItemDecoration(itemDecoration);

        mRefreshLayout.setOnRefreshListener(mMyLooksPresenter.getRefreshListener());

        // Setup pagination
        mPagination = mMyLooksPresenter.getPaginater(mLooksRecycler);

        return layout;
    }

    @Override
    public void onComplete() {
        if (mMyLooksAdapter.getItemCount() == 0) {
            mNoPostsTxt.setVisibility(View.VISIBLE);
        } else {
            mNoPostsTxt.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError() {
        mPagination.unbind();
        Toast.makeText(getContext(), getString(R.string.could_not_refresh),
                Toast.LENGTH_SHORT).show();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemsRetrieved(List<Post> posts) {
        mMyLooksAdapter.appendPosts(posts);
        mMyLooksAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishRefreshing() {
        mMyLooksAdapter.clearAll();
        mRefreshLayout.setRefreshing(false);
    }

    /**
     * Called from the InspirationPresenter when a post has been liked
     * @param post
     */
    public void liked(Post post) {
        mMyLooksAdapter.addPost(post);
        mMyLooksAdapter.notifyDataSetChanged();
        onComplete();
    }

    /**
     * Called from the InspirationPresenter when a post has been unliked
     * @param post
     */
    public void unliked(Post post) {
        mMyLooksAdapter.removePost(post);
        mMyLooksAdapter.notifyDataSetChanged();
        onComplete();
    }
}
