package au.com.kentandlime.pocketstylist.activity;

/**
 * Defines the callback contract that each Paginateable and Refreshable View must implement
 * This is so the presenter is able to call each callback to update the view
 * Abstract Type T should be the type of the data that the view accepts e.g. Post
 */
public interface RefreshablePaginateableContentView<T> {
    /**
     * Is called by the presenter when the request to the API has finished
     */
    void onComplete();

    /**
     * Is called by the presenter when the API errors
     */
    void onError();

    /**
     * Called from the presenter when the list is finished refreshing
     */
    void finishRefreshing();

    /**
     * Called by the presenter when there are new data items loaded from the API
     */
    void onItemsRetrieved(T data);
}
