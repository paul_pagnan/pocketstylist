package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.Date;

/**
 * An immutable chat message model. Defines all the attributes necessary for communication between
 * user and the stylist
 */
public class ChatMessage extends SugarRecord {
    @SerializedName("user")
    private String mUserId;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("from")
    private UserType mFrom;

    @SerializedName("stylist")
    private String stylist;

    @SerializedName("seen")
    private boolean mSeen;

    private Date mReceivedAt = new Date();

    public ChatMessage() {
    }

    public ChatMessage(String message, UserType from) {
        mMessage = message;
        mFrom = from;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public UserType getFrom() {
        return mFrom;
    }

    public void setFrom(UserType from) {
        mFrom = from;
    }

    public String getStylist() {
        if (stylist == null) return User.DEFAULT_STYLIST;
        return stylist;
    }

    public void setStylist(String stylist) {
        this.stylist = stylist;
    }

    public boolean isSeen() {
        return mSeen;
    }

    public void setSeen(boolean seen) {
        mSeen = seen;
    }

    /**
     * Enum to define the type of user that sent the message, was it from the Stylist
     * or the Customer
     */
    public enum UserType {
        @SerializedName("stylist")
        STYLIST,
        @SerializedName("user")
        USER
    }
}
