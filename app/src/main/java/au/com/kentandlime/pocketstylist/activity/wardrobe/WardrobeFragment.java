package au.com.kentandlime.pocketstylist.activity.wardrobe;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.RefreshablePaginateableContentView;
import au.com.kentandlime.pocketstylist.adapter.ItemOffsetDecoration;
import au.com.kentandlime.pocketstylist.adapter.WardrobeAdapter;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import au.com.kentandlime.pocketstylist.service.WardrobeUploadService;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by paul on 25/09/16
 * The fragment that shows the list of items uploaded by the user.
 */
public class WardrobeFragment extends Fragment
        implements RefreshablePaginateableContentView<List<WardrobeItem>>{
    public static final int KEY_TAKE_PHOTO = 0;
    public static final int KEY_CHOOSE_FROM_GALLERY = 1;
    public static final int KEY_WARDROBE_ADDED = 2;
    private static final int REQUEST_VIEW_WARDROBE = 3;

    @BindView(R.id.wardrobe_fab)
    FloatingActionButton mFloatingActionButton;

    @BindView(R.id.wardrobe_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.wardrobe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.wardrobe_no_items)
    TextView mEmptyTxt;

    @BindView(R.id.wardrobe_error_loading)
    TextView mErrorTxt;

    private String mImageUri;
    private WardrobeAdapter mWardrobeAdapter;
    private WardrobePresenter mPresenter;
    private Paginate mPagination;
    private BroadcastReceiver mBroadCastReceiver;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.my_wardrobe));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment and inject dependencies
        View view = inflater.inflate(R.layout.fragment_wardrobe, container, false);
        ButterKnife.bind(this, view);
        App.getInjectorComponent().inject(this);
        mPresenter = new WardrobePresenter(this, getContext());

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showImageOptionsDialog(new WardrobePresenter.ImageProvider() {
                    @Override
                    public void fromCamera() {
                        takePhoto();
                    }

                    @Override
                    public void fromGallery() {
                        chooseFromGallery();
                    }
                });
            }
        });

        mWardrobeAdapter = new WardrobeAdapter(getContext());
        mWardrobeAdapter.setOnClickListener(new WardrobeAdapter.OnClickListener() {
            @Override
            public void onClick(WardrobeItem wardrobeItem) {
                gotoViewActivity(wardrobeItem);
            }
        });

        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),
                ItemOffsetDecoration.NUM_COLUMNS));
        mRecyclerView.setAdapter(mWardrobeAdapter);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(
                getContext(),
                R.dimen.item_offset,
                ItemOffsetDecoration.NUM_COLUMNS);
        mRecyclerView.addItemDecoration(itemDecoration);

        mSwipeRefreshLayout.setOnRefreshListener(mPresenter.getRefreshListener());

        // Setup pagination
        mPagination = mPresenter.getPaginater(mRecyclerView);

        // Register the broadcast receiver for when the service has completed the upload
        getActivity().registerReceiver(getBroadcastReceiver(),
                new IntentFilter(WardrobeUploadService.WARDROBE_UPDATE));

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(getBroadcastReceiver());
    }

    /**
     * Navigates to the ViewWardrobeActivity when the user clicks a tile in the wardrobe view
     * @param wardrobeItem
     */
    private void gotoViewActivity(WardrobeItem wardrobeItem) {
        Intent intent = new Intent(getContext(), ViewWardrobeActivity.class);
        intent.putExtra(ViewWardrobeActivity.KEY_WARDROBE_ITEM, wardrobeItem);
        startActivityForResult(intent, REQUEST_VIEW_WARDROBE);
    }

    /**
     * Setup the location to store the image temporarily
     */
    private File createImageFile() {
        try {
            String fileName = UUID.randomUUID().toString();
            File directory = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(fileName, WardrobeUploadService.FILE_EXTENSION,
                    directory);
            mImageUri = imageFile.getAbsolutePath();
            return imageFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Shows the file picker so that the user can choose an existing photo
     */
    private void chooseFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        String title = getString(R.string.select_an_image);
        startActivityForResult(Intent.createChooser(intent, title), KEY_CHOOSE_FROM_GALLERY);
    }

    /**
     * Shows the camera to allow the user to take a photo
     */
    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            photoFile = createImageFile();

            if (photoFile != null) {
                Uri contentUri = FileProvider.getUriForFile(getContext(),
                        getActivity().getApplicationContext().getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                startActivityForResult(takePictureIntent, KEY_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            startAddWardrobeActivity(Uri.fromFile(new File(mImageUri)), false);
        } else if (requestCode == KEY_CHOOSE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            startAddWardrobeActivity(data.getData(), true);
        } else if (requestCode == KEY_WARDROBE_ADDED && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getContext(), R.string.upload_started,
                    Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_VIEW_WARDROBE
                && resultCode == ViewWardrobeActivity.RESULT_DELETED) {
            WardrobeItem wardrobeItem =
                    data.getParcelableExtra(ViewWardrobeActivity.KEY_WARDROBE_ITEM);
            mWardrobeAdapter.remove(wardrobeItem);
            mWardrobeAdapter.notifyDataSetChanged();
            onComplete();
        }
    }

    /**
     * Starts the add wardrobe activity so the user can tag the image that they have selected
     */
    private void startAddWardrobeActivity(Uri imageUri, boolean isContentUri) {
        Intent intent = new Intent(getContext(), AddWardrobeActivity.class);
        intent.putExtra(AddWardrobeActivity.KEY_WARDROBE_IMAGE, imageUri);
        intent.putExtra(AddWardrobeActivity.KEY_IS_CONTENT_URI, isContentUri);
        startActivityForResult(intent, KEY_WARDROBE_ADDED);
    }

    @Override
    public void finishRefreshing() {
        mWardrobeAdapter.clear();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemsRetrieved(List<WardrobeItem> data) {
        mWardrobeAdapter.appendItems(data);
        mWardrobeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError() {
        if (mWardrobeAdapter.getItemCount() == 0) {
            mErrorTxt.setVisibility(View.VISIBLE);
        } else {
            mErrorTxt.setVisibility(View.GONE);
        }
        mEmptyTxt.setVisibility(View.GONE);
        Toast.makeText(getContext(), R.string.could_not_refresh, Toast.LENGTH_SHORT).show();
        mPagination.unbind();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onComplete() {
        mErrorTxt.setVisibility(View.GONE);
        if (mWardrobeAdapter.getItemCount() == 0) {
            mEmptyTxt.setVisibility(View.VISIBLE);
        } else {
            mEmptyTxt.setVisibility(View.GONE);
        }
    }

    /**
     * Returns the BroadCast receiver which catches the broadcast fired from the service in order
     * to notify this view that the upload has finished
     * @return
     */
    public BroadcastReceiver getBroadcastReceiver() {
        if (mBroadCastReceiver == null) {
            mBroadCastReceiver =  new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    WardrobeItem wardrobeItem =
                            intent.getParcelableExtra(WardrobeUploadService.KEY_WARDROBE);

                    mWardrobeAdapter.prependItem(wardrobeItem);
                    mWardrobeAdapter.notifyDataSetChanged();
                    onComplete();
                }
            };
        }
        return mBroadCastReceiver;
    }
}
