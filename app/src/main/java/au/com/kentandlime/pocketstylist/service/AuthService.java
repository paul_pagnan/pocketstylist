package au.com.kentandlime.pocketstylist.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.model.AuthResponse;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.api.ApiServiceFactory;
import au.com.kentandlime.pocketstylist.service.api.AuthInterface;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Service to authenticate the user
 */
public class AuthService {
    public static final String AUTH_SHARED_PREFS_KEY = "AUTH_DETAILS";
    public static final String JWT_SHARED_PREFS_KEY = "JWT";
    public static final String EMAIL_SHARED_PREFS_KEY = "CURRENT_EMAIL";
    private static final String ONBOARDING_DONE = "ONBOARDING_DONE";
    private final AuthInterface authService;

    private static String sUserJwt;

    @Inject
    Context mContext;

    /**
     * Instantiate the AuthService and create the Retrofit Service from the ServiceFactory
     */
    public AuthService() {
        App.getInjectorComponent().inject(this);

        authService = ApiServiceFactory.createRetrofitService(
                AuthInterface.class,
                AuthInterface.SERVICE_ENDPOINT);
    }

    /**
     * Authenticates the users login information against the customer portal API, stores the
     * returned JWT in shared preferences to be used by other services later
     * @param email
     * @param password
     * @return
     */
    public Observable<AuthResponse> authenticateUser(final String email, String password) {
        Observable<AuthResponse> authResponseObservable =
                authService.authenticateUser(email, password)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        return authResponseObservable.flatMap(new Func1<AuthResponse, Observable<AuthResponse>>() {
            @Override
            public Observable<AuthResponse> call(AuthResponse authResponse) {
                SharedPreferences prefs = mContext.getSharedPreferences(
                        AUTH_SHARED_PREFS_KEY, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(JWT_SHARED_PREFS_KEY, authResponse.getToken());
                editor.putString(EMAIL_SHARED_PREFS_KEY, email);
                editor.apply();

                sUserJwt = authResponse.getToken();

                return Observable.just(authResponse);
            }
        });
    }

    /**
     * Determines if the user is logged in by checking that a JWT is present in SharedPrefs
     * @return
     */
    public boolean isLoggedIn() {
        return getJwt() != null;
    }

    /**
     * Gets the current user data by decoding the JWT received during auth
      * @return
     */
    public User getUser() {
        String[] jwtParts = getJwt().split("\\.");
        byte[] decodedBytes = Base64.decode(jwtParts[1], Base64.NO_WRAP);

        try {
            String jsonString = new String(decodedBytes, "UTF-8");
            Gson gson = new Gson();
            return gson.fromJson(jsonString, User.class);
        } catch(UnsupportedEncodingException e) {
            Log.d(null, e.getMessage());
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Get JWT from field if exists, otherwise retrieve it from Shared Prefs
     * @return
     */
    private String getJwt() {
        if(sUserJwt == null) {
            SharedPreferences prefs = mContext.getSharedPreferences(
                    AUTH_SHARED_PREFS_KEY, Context.MODE_PRIVATE);
            sUserJwt = prefs.getString(JWT_SHARED_PREFS_KEY, null);
        }
        return sUserJwt;
    }

    /**
     * Logs the user out by clearing the auth data from Shared Prefs
     */
    public void logOut() {
        SharedPreferences prefs = mContext.getSharedPreferences(
                AUTH_SHARED_PREFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(JWT_SHARED_PREFS_KEY);
        editor.remove(EMAIL_SHARED_PREFS_KEY);
        sUserJwt = null;
        editor.apply();
    }

    /**
     * Checks shared preferences to see if the ONBOARDING_DONE flag is false
     * @return
     */
    public boolean doesNeedOnboarding() {
        SharedPreferences prefs = mContext.getSharedPreferences(
                AUTH_SHARED_PREFS_KEY, Context.MODE_PRIVATE);
        return !prefs.getBoolean(ONBOARDING_DONE, false);
    }

    /**
     * Sets the ONBOARDING_DONE flag to true in shared preferences once the user has read the
     * onboarding instructions
     */
    public void finishOnboarding() {
        SharedPreferences prefs = mContext.getSharedPreferences(
                AUTH_SHARED_PREFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ONBOARDING_DONE, true);
        editor.apply();
    }
}
