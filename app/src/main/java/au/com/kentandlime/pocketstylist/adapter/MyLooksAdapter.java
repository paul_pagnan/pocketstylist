package au.com.kentandlime.pocketstylist.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Defines the gridview adapter for the my looks screen
 */
public class MyLooksAdapter extends RecyclerView.Adapter<MyLooksAdapter.ViewHolder> {
    private final Context mContext;
    private final RecyclerView mRecyclerView;
    private List<Post> mPosts = new ArrayList<>();
    private Dialog mQuickViewBuilder;
    private static LayoutInflater sInflater = null;

    public MyLooksAdapter(Context context, RecyclerView recyclerView) {
        sInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        mRecyclerView = recyclerView;
        setupTouchListeners();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = sInflater.inflate(R.layout.list_item_look_thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Post post = mPosts.get(position);

        if (post != null) {
            Picasso.with(mContext).load(post.getImageUrl()).into(holder.mLookImageView);
            holder.mLookImageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    showPostQuickView(post);
                    mRecyclerView.getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                }
            });
        }
    }

    /**
     * Sets up the route touch listeners to control the show/hide of the quick preview
     */
    private void setupTouchListeners() {
        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_UP) {
                    mRecyclerView.setNestedScrollingEnabled(true);
                    hideQuickView();
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent event) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });

        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                    hideQuickView();
                return false;
            }
        });
    }

    /**
     * Hides the post quick view
     */
    public void hideQuickView(){
        mRecyclerView.getParent().requestDisallowInterceptTouchEvent(false);
        if(mQuickViewBuilder != null) mQuickViewBuilder.dismiss();
    }

    /**
     * Creates the post quick view dialog and shows it
     * @param post
     */
    public void showPostQuickView(Post post){
        View view = sInflater.inflate(R.layout.post_quick_view, null);

        ImageView lookImage = (ImageView) view.findViewById(R.id.quick_view_look_image);
        ImageView stylistImage = (ImageView) view.findViewById(R.id.quick_view_stylist_image);
        TextView stylistName = (TextView) view.findViewById(R.id.quick_view_stylist_text);

        Picasso.with(mContext).load(post.getImageUrl()).into(lookImage);
        Picasso.with(mContext).load(User.getStylistImageUrl(post.getStylist())).into(stylistImage);

        stylistName.setText(post.getStylist());

        mQuickViewBuilder = new Dialog(mContext);
        mQuickViewBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mQuickViewBuilder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        mQuickViewBuilder.setContentView(view);
        mQuickViewBuilder.show();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mPosts == null) return 0;
        return mPosts.size();
    }

    /**
     * Appends a list of posts to the end of the list
     * @param posts
     */
    public void appendPosts(List<Post> posts) {
        if (posts != null) {
            mPosts.addAll(posts);
        }

    }

    /**
     * Clears all items from the list
     */
    public void clearAll() {
        mPosts.clear();
    }

    /**
     * Adds a single post to top of the like list, used for when a post is liked
     * @param post
     */
    public void addPost(Post post) {
        mPosts.add(0, post);
    }

    /**
     * Removes a specific post from the list, used for when a post is unliked
     * @param post
     */
    public void removePost(Post post) {
        Iterator<Post> iterator = mPosts.iterator();
        while(iterator.hasNext())
        {
            Post nextPost = iterator.next();
            if (nextPost.getId().equals(post.getId())) {
                iterator.remove();
            }
        }
    }

    /**
     * The view holder for each look thumbnail
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.look_thumbnail_image)
        ImageView mLookImageView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
