package au.com.kentandlime.pocketstylist.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * POJO to define a Wardrobe item, used for API requests
 */
public class WardrobeItem implements Parcelable {
    @SerializedName("_id")
    private String mId;

    @SerializedName("brand")
    private String mBrand;

    @SerializedName("size")
    private String mSize;

    @SerializedName("category")
    private String mCategory;

    @SerializedName("imageUrl")
    private String mImageUrl;

    @SerializedName("thumbnailImageUrl")
    private String mThumbnailImageUrl;

    @SerializedName("likeReasons")
    private List<String> mLikeReasons;

    public WardrobeItem(String id, String brand, String size, String category, String imageUrl,
                        String thumbnailImageUrl, List<String> likeReasons) {
        mId = id;
        mBrand = brand;
        mSize = size;
        mCategory = category;
        mImageUrl = imageUrl;
        mThumbnailImageUrl = thumbnailImageUrl;
        mLikeReasons = likeReasons;
    }

    /**
     * Create the wardrobe item from Parecelable input
     * @param in
     */
    protected WardrobeItem(Parcel in) {
        mId = in.readString();
        mBrand = in.readString();
        mSize = in.readString();
        mCategory = in.readString();
        mImageUrl = in.readString();
        mThumbnailImageUrl = in.readString();
        mLikeReasons = in.createStringArrayList();
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String brand) {
        mBrand = brand;
    }

    public String getSize() {
        return mSize;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getThumbnailImageUrl() {
        return mThumbnailImageUrl;
    }

    public void setThumbnailImageUrl(String thumbnailImageUrl) {
        mThumbnailImageUrl = thumbnailImageUrl;
    }

    public List<String> getLikeReasons() {
        return mLikeReasons;
    }

    public void setLikeReasons(List<String> likeReasons) {
        mLikeReasons = likeReasons;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mBrand);
        dest.writeString(mSize);
        dest.writeString(mCategory);
        dest.writeString(mImageUrl);
        dest.writeString(mThumbnailImageUrl);
        dest.writeStringList(mLikeReasons);
    }

    public static final Creator<WardrobeItem> CREATOR = new Creator<WardrobeItem>() {
        @Override
        public WardrobeItem createFromParcel(Parcel in) {
            return new WardrobeItem(in);
        }

        @Override
        public WardrobeItem[] newArray(int size) {
            return new WardrobeItem[size];
        }
    };
}
