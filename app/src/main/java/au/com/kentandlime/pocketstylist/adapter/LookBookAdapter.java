package au.com.kentandlime.pocketstylist.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.CommentDialogFragment;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.model.User;

/**
 * Defines the list adapter for an individual post
 */
public class LookBookAdapter extends RecyclerView.Adapter<LookBookAdapter.ViewHolder> {
    private static final TimeInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final TimeInterpolator ACCELERATE_INTERPOLATOR = new AccelerateDecelerateInterpolator();

    private Context mContext;
    private FragmentManager mFragmentManager;
    private List<Post> mPosts;
    private static LayoutInflater mInflater = null;
    private OnPostLikeListener mLikeListener;

    public LookBookAdapter(Context context, FragmentManager fragmentManager) {
        mContext = context;
        mFragmentManager = fragmentManager;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPosts = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_look, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Post post = mPosts.get(position);

        // Set post attributes
        holder.mDescription.setText(post.getDescription());
        holder.mStylist.setText(post.getStylist());
        updateLikes(holder, post);

        holder.mLikeButton.setLiked(post.isLiked());

        PrettyTime prettyTime = new PrettyTime();
        DateTime localDate = LocalDateTime.fromDateFields(post.getCreatedAt())
                .toDateTime(DateTimeZone.UTC);
        holder.mTime.setText(prettyTime.format(localDate.toDate()));

        Picasso.with(mContext).load(post.getImageUrl()).into(holder.mLookImageView);

        Picasso.with(mContext).load(User.getStylistImageUrl(post.getStylist()))
                .into(holder.mStylistImage);

        // Set event listeners
        holder.mLikeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (mLikeListener != null) mLikeListener.liked(post);
                updateLikes(holder, post);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                if (mLikeListener != null) mLikeListener.unLiked(post);
                updateLikes(holder, post);
            }
        });

        final GestureListener gestureListener = new GestureListener(
                holder.mLikeButton,
                holder.mCircleBackground,
                holder.mHeartImageView,
                post,
                holder);
        final GestureDetector gestureDetector = new GestureDetector(mContext, gestureListener);

        holder.mLookImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        holder.mInteractionCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCommentDialog(false, post);
            }
        });

        holder.mCommentImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCommentDialog(true, post);
            }
        });
    }

    private void updateLikes(ViewHolder holder, Post post) {
        holder.mCommentsNumber.setText(Integer.toString(post.getComments()));
        holder.mLikesNumber.setText(Integer.toString(post.getLikes()));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mPosts == null) return 0;
        return mPosts.size();
    }

    /**
     * Sets the on like listener callback
     * @param likeListener
     */
    public void setOnLikeListener(OnPostLikeListener likeListener) {
        mLikeListener = likeListener;
    }

    /**
     * Shows the dialog that allows the user to comment on the post and view likes
     */
    private void showCommentDialog(boolean focusCommentInput, Post post) {
        CommentDialogFragment commentDialog = new CommentDialogFragment.Builder()
                .setFocusCommentInput(focusCommentInput)
                .setPost(post)
                .setOnUpdateCommentListener(getOnUpdateCommentListener(post))
                .build();

        commentDialog.show(mFragmentManager, null);
    }

    public void appendPosts(List<Post> posts) {
        if (posts != null) mPosts.addAll(posts);
    }

    /**
     * Clears all posts from the list
     */
    public void clearAll() {
        mPosts.clear();
    }

    /**
     * Updates the view when there are new comments for the current post
     * @return
     */
    public CommentDialogFragment.OnUpdateComments getOnUpdateCommentListener(final Post post) {
        return new CommentDialogFragment.OnUpdateComments() {
            @Override
            public void update(int numberOfComments) {
                post.setComments(numberOfComments);
                notifyDataSetChanged();
            }
        };
    }

    /**
     * The view holder for each look item
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        private final LikeButton mLikeButton;
        private final View mCircleBackground;
        private final RelativeLayout mInteractionCounter;
        private final ImageView mCommentImageView;
        private final ImageView mHeartImageView;
        private final ImageView mLookImageView;

        private final TextView mDescription;
        private final TextView mStylist;
        private final TextView mLikesNumber;
        private final TextView mCommentsNumber;
        private final TextView mTime;
        private final ImageView mStylistImage;

        public ViewHolder(View v) {
            super(v);
            mLikeButton = (LikeButton) v.findViewById(R.id.like_button);
            mLookImageView = (ImageView) v.findViewById(R.id.look_image);
            mHeartImageView = (ImageView) v.findViewById(R.id.like_heart);
            mInteractionCounter = (RelativeLayout) v.findViewById(
                    R.id.look_post_interaction_counter_container);
            mCommentImageView = (ImageView) v.findViewById(R.id.look_post_comment_image);
            mCircleBackground = v.findViewById(R.id.like_circle_bg);

            mDescription = (TextView) v.findViewById(R.id.look_description_text);
            mStylist = (TextView) v.findViewById(R.id.look_stylist_text);
            mLikesNumber = (TextView) v.findViewById(R.id.look_likes_number);
            mCommentsNumber = (TextView) v.findViewById(R.id.look_comments_number);
            mTime = (TextView) v.findViewById(R.id.look_time_text);
            mStylistImage = (ImageView) v.findViewById(R.id.look_stylist_image);
        }
    }

    /**
     * On double tap of the image view, animate the heart image overlay
     * @param circleBackground
     * @param heartImageView
     */
    private void animateLike(final View circleBackground, final View heartImageView) {
        circleBackground.setVisibility(View.VISIBLE);
        heartImageView.setVisibility(View.VISIBLE);

        circleBackground.setScaleY(0.1f);
        circleBackground.setScaleX(0.1f);
        circleBackground.setAlpha(1f);
        heartImageView.setScaleY(0.5f);
        heartImageView.setScaleX(0.5f);
        heartImageView.setAlpha(0.1f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(circleBackground, "scaleY", 0.1f, 1f);
        bgScaleYAnim.setDuration(200);
        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(circleBackground, "scaleX", 0.1f, 1f);
        bgScaleXAnim.setDuration(200);
        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(circleBackground, "alpha", 1f, 0f);
        bgAlphaAnim.setDuration(200);
        bgAlphaAnim.setStartDelay(150);
        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 0.5f, 1.15f);
        imgScaleUpYAnim.setDuration(200);
        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 0.5f, 1.15f);
        imgScaleUpXAnim.setDuration(200);
        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator heartAlphaAnim = ObjectAnimator.ofFloat(heartImageView, "alpha", 0.1f, 1f);
        heartAlphaAnim.setDuration(200);
        heartAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 1.15f, 0.5f);
        imgScaleDownYAnim.setDuration(200);
        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 1.15f, 0.5f);
        imgScaleDownXAnim.setDuration(200);
        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
        ObjectAnimator heartAlphaDownAnim = ObjectAnimator.ofFloat(heartImageView, "alpha", 1f, 0f);
        heartAlphaDownAnim.setDuration(200);
        heartAlphaDownAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim, heartAlphaAnim);
        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).with(heartAlphaDownAnim).after(imgScaleUpYAnim);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                circleBackground.setVisibility(View.GONE);
                heartImageView.setVisibility(View.GONE);
            }
        });
        animatorSet.start();
    }

    /**
     * Gesture listener to detect double tap on the main content image
     * Once tapped, do the like animation
     */
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private final View mHeartImageView;
        private final View mCircleBackground;
        private final LikeButton mLikeButton;
        private final Post mPost;
        private final ViewHolder mHolder;

        public GestureListener(LikeButton likeButton, View circleBackground, View heartImageView,
                               Post post, ViewHolder holder) {
            super();
            mCircleBackground = circleBackground;
            mHeartImageView = heartImageView;
            mLikeButton = likeButton;
            mPost = post;
            mHolder = holder;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (!mPost.isLiked()) {
                if (mLikeListener != null) mLikeListener.liked(mPost);
                updateLikes(mHolder, mPost);
            }

            animateLike(mCircleBackground, mHeartImageView);
            mLikeButton.setLiked(true);

            return true;
        }
    }

    /**
     * The interface to define the callbacks for when a post is liked or unliked
     */
    public interface OnPostLikeListener {
        /**
         * Called when any post is liked
         * @param post
         */
        void liked(Post post);

        /**
         * Called when any post is unliked
         * @param post
         */
        void unLiked(Post post);
    }
}
