package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model to define a successful authentication response from CPAPI
 */
public class AuthResponse {
    @SerializedName("token")
    private String mToken;

    public AuthResponse(String token) {
        mToken = token;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }
}
