package au.com.kentandlime.pocketstylist.activity.inspiration.mylooks;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.activity.BasePresenter;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.service.ContentService;


/**
 * Presenter to provide data to the My Looks Fragment view
 */
public class MyLooksPresenter extends BasePresenter<List<Post>> {
    @Inject
    ContentService mContentService;

    private final MyLooksFragment mView;

    /**
     * Instantiates the Inspiration presenter, injects dependencies and configures base presenter
     * @param view
     */
    public MyLooksPresenter(MyLooksFragment view) {
        App.getInjectorComponent().inject(this);
        mView = view;
        setDataProvider(getDataProvider());
    }

    /**
     * Loads the posts from the API and calls the callbacks on the view
     */
    private void getMyLooks() {
        startLoading();
        mContentService.getMyLooks(getCurrentPage()).subscribe(getSubscriber(mView));
    }

    /**
     * Returns a new data provider that defines what happens when the listeners request more data
     * @return
     */
    private DataProvider getDataProvider() {
        return new DataProvider() {
            @Override
            public void loadMoreData() {
                getMyLooks();
            }
        };
    }
}
