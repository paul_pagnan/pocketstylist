package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Wrapper model for any request that requires pagination
 */
public class PaginatedResponse<T> {
    @SerializedName("data")
    private T mData;

    @SerializedName("count")
    private int mCount;

    @SerializedName("isLastPage")
    private boolean mIsLastPage;

    public PaginatedResponse(T data, int count, boolean isLastPage) {
        mData = data;
        mCount = count;
        mIsLastPage = isLastPage;
    }

    public T getData() {
        return mData;
    }

    public void setData(T data) {
        mData = data;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public boolean isLastPage() {
        return mIsLastPage;
    }

    public void setLastPage(boolean lastPage) {
        mIsLastPage = lastPage;
    }
}
