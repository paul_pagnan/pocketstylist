package au.com.kentandlime.pocketstylist.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Adds small borders between each item in the grid
 */
public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
    public static final int NUM_COLUMNS = 3;
    private int mSpanCount;
    private int mItemOffset;

    public ItemOffsetDecoration(int itemOffset, int spanCount) {
        mItemOffset = itemOffset;
        mSpanCount = spanCount;
    }

    public ItemOffsetDecoration(@NonNull Context context,
                                @DimenRes int itemOffsetId,
                                int spanCount) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId), spanCount);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);
        int column = position % mSpanCount;

        int leftOffset = mItemOffset - column * mItemOffset / mSpanCount;
        int rightOffset = (column + 1) * mItemOffset / mSpanCount;
        int topOffset = position < mSpanCount ? mItemOffset : 0;

        outRect.set(leftOffset, topOffset, rightOffset, mItemOffset);
    }
}