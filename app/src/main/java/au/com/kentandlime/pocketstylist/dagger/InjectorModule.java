package au.com.kentandlime.pocketstylist.dagger;


import android.content.Context;

import javax.inject.Singleton;

import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationPresenter;
import au.com.kentandlime.pocketstylist.activity.login.LoginPresenter;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.ChatService;
import au.com.kentandlime.pocketstylist.service.ContentService;
import au.com.kentandlime.pocketstylist.util.DialogUtil;
import dagger.Module;
import dagger.Provides;

/**
 * Defines the dagger injector module for the app
 */
@Module
public class InjectorModule {
    @Provides
    @Singleton
    LoginPresenter provideLoginPresenter() {
        return new LoginPresenter();
    }

    @Provides
    @Singleton
    InspirationPresenter provideInspirationPresenter(Context context) {
        return new InspirationPresenter(context);
    }

    @Provides
    @Singleton
    public AuthService providesAuthService() {
        return new AuthService();
    }

    @Provides
    @Singleton
    public ContentService providesContentService(Context context) {
        return new ContentService(context);
    }

    @Provides
    @Singleton
    public ChatService providesChatService() {
        return new ChatService();
    }

    @Provides
    @Singleton
    public DialogUtil providesDialogUtil(Context context) {
        return new DialogUtil(context);
    }
}
