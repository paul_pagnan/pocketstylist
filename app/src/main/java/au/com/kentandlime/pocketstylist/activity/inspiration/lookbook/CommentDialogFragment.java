package au.com.kentandlime.pocketstylist.activity.inspiration.lookbook;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDateTime;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.adapter.CommentsAdapter;
import au.com.kentandlime.pocketstylist.model.Comment;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.AuthService;
import au.com.kentandlime.pocketstylist.service.ContentService;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;

/**
 * The dialog box that allows the user to interact with a post. Allows them to comment
 * see who liked the post etc
 */
public class CommentDialogFragment extends DialogFragment implements View.OnTouchListener {
    private static final String FOCUS_COMMENT_INPUT_KEY = "focusCommentInput";
    private static final String POST_ID = "postId";

    @Inject
    ContentService mContentService;

    @Inject
    AuthService mAuthService;

    @BindView(R.id.comment_edit_text)
    EditText mCommentEt;

    @BindView(R.id.comment_number_likes)
    TextView mNumLikes;

    @BindView(R.id.comment_no_comments)
    TextView mNoComments;

    @BindView(R.id.comment_post_btn)
    ImageView mPostBtn;

    @BindView(R.id.comment_back_btn)
    ImageView mBackBtn;

    @BindView(R.id.comment_recycler_view)
    RecyclerView mCommentsRecycler;

    @BindView(R.id.comment_progress)
    ProgressBar mProgressView;

    private boolean mIsScrollingDown = false;
    private static final String NUM_LIKES = "focusCommentInput";
    private View mDialogContainer;
    private int mPreviousFingerPosition = 0;

    private int mDialogContainerPosition = 0;
    private int mDefaultViewHeight;

    private boolean mIsClosing = false;

    private GestureDetector mGestureListener;
    private CommentsAdapter mCommentAdapter;
    private String mPostId;
    private User mUser;
    private OnUpdateComments mOnUpdateCommentListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_comments, container, false);
        App.getInjectorComponent().inject(this);
        ButterKnife.bind(this, view);

        mUser = mAuthService.getUser();

        setupViewLogic();
        setupCommentView(view);

        // set keyboard behaviour
        getDialog().getWindow().
                setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // make background transparent, we control background color in the layout
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // setup touch listeners
        mDialogContainer = getDialog().getWindow().getDecorView();
        getDialog().getWindow().getDecorView().setOnTouchListener(this);
        mGestureListener = new GestureDetector(new GestureListener());

        return view;
    }

    /**
     * Initialises the comments recycker view and assigns it's adapter
     * @param view
     */
    private void setupCommentView(View view) {
        mCommentsRecycler = (RecyclerView) view.findViewById(R.id.comment_recycler_view);

        loadComments();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        mCommentsRecycler.setLayoutManager(layoutManager);
        mCommentAdapter = new CommentsAdapter(getContext());
        mCommentsRecycler.setAdapter(mCommentAdapter);
    }

    /**
     * Loads the comments for the post
     */
    private void loadComments() {
        mPostId = getArguments().getString(POST_ID);
        mContentService.getComments(mPostId)
            .subscribe(new Subscriber<List<Comment>>() {
                @Override
                public void onCompleted() {
                    mProgressView.setVisibility(View.GONE);
                    if (mCommentAdapter.getItemCount() == 0) {
                        mNoComments.setVisibility(View.VISIBLE);
                    } else {
                        mNoComments.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(Throwable e) {
                    Toast.makeText(getContext(), getString(R.string.failed_to_load_comments),
                            Toast.LENGTH_SHORT).show();
                    onCompleted();
                }

                @Override
                public void onNext(List<Comment> comments) {
                    mCommentAdapter.appendAll(comments);
                    mOnUpdateCommentListener.update(comments.size());
                }
            });
    }

    /**
     * Posts a comment via the content service
     */
    private void postComment(final String comment) {
        mContentService.postComment(mPostId, comment)
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        mPostBtn.setImageDrawable(getResources().getDrawable(R.drawable.send));
                        mPostBtn.setClickable(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), getString(R.string.unable_to_comment),
                                Toast.LENGTH_SHORT).show();
                        onCompleted();
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        mCommentAdapter.append(new Comment(mUser, comment,
                                LocalDateTime.now().toDate()));
                        mCommentAdapter.notifyDataSetChanged();
                        mCommentEt.setText(null);
                        mNoComments.setVisibility(View.GONE);
                        mOnUpdateCommentListener.update(mCommentAdapter.getItemCount());
                    }
                });
    }

    /**
     * Configures the view logic for actions such as click events etc
     */
    public void setupViewLogic() {
        boolean focusCommentInput = getArguments().getBoolean(FOCUS_COMMENT_INPUT_KEY);
        int numLikes = getArguments().getInt(NUM_LIKES);

        mNumLikes.setText(Integer.toString(numLikes).concat(" ").concat(getString(R.string.likes)));

        if (focusCommentInput) {
            mCommentEt.requestFocus();
        }

        mCommentEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().isEmpty()) {
                    mPostBtn.setImageDrawable(getResources().getDrawable(R.drawable.send_disabled));
                    mPostBtn.setClickable(false);
                } else {
                    mPostBtn.setImageDrawable(getResources().getDrawable(R.drawable.send));
                    mPostBtn.setClickable(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPostBtn.setImageDrawable(getResources().getDrawable(R.drawable.send_disabled));
                mPostBtn.setClickable(false);
                postComment(mCommentEt.getText().toString().trim());
            }
        });

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateDismissDialog();
            }
        });
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int fingerY = (int) event.getRawY();

        // Pipe event to gesture listener to detect fling gesture
        if (mGestureListener.onTouchEvent(event)) {
            return true;
        }

        // Switch on motion event type
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                // save default base layout height
                mDefaultViewHeight = mDialogContainer.getHeight();

                // Init finger and view position
                mPreviousFingerPosition = fingerY;
                mDialogContainerPosition = (int) mDialogContainer.getY();
                return true;
            case MotionEvent.ACTION_UP:
                handleFingerUp();
                return true;

            case MotionEvent.ACTION_MOVE:
                handleMove(fingerY);
                mPreviousFingerPosition = fingerY;
                return true;
        }
        return true;
    }

    /**
     * When the user moves their finger on the dialog, move the dialog and close it if it reaches
     * passed the threshold
     */
    private void handleMove(int fingerY) {
        if (mIsClosing) return;
        int currentYPosition = (int) mDialogContainer.getY();

        // If user is scrolling up
        if(mPreviousFingerPosition > fingerY){
            mIsScrollingDown = false;
        }
        else{
            mIsScrollingDown = true;
        }

        // User has scrolled passed the threshold. Close the dialog
        if (Math.abs(mDialogContainerPosition - currentYPosition) > mDefaultViewHeight / 1.5 )
        {
            animateDismissDialog();
            return;
        }

        mDialogContainer.setY(mDialogContainer.getY() + (fingerY - mPreviousFingerPosition));
    }

    /**
     * Reset the dialog back to it's default position
     */
    private void handleFingerUp() {
        if (!mIsClosing) {
            ObjectAnimator resetYAnimator = ObjectAnimator.ofFloat(mDialogContainer, "y",
                    mDialogContainer.getY(),
                    0);
            resetYAnimator.setDuration(300);
            resetYAnimator.start();
        }

        mDialogContainer.getLayoutParams().height = mDefaultViewHeight;
        mDialogContainer.requestLayout();

        mIsScrollingDown = false;
    }

    /**
     * Animate the dismissal of the dialog
     */
    public void animateDismissDialog(){
        mIsClosing = true;

        Display display = getDialog().getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight = size.y;

        int yOffset = mIsScrollingDown ?
                screenHeight + mDialogContainer.getHeight() :
                -screenHeight - mDialogContainer.getHeight();

        ObjectAnimator positionAnimator = ObjectAnimator.ofFloat(mDialogContainer, "y",
                mDialogContainer.getY(), yOffset);
        positionAnimator.setDuration(300);
        positionAnimator.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator)
            {
                dismiss();
                mIsClosing = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        positionAnimator.start();
    }

    /**
     * Sets the OnUpdateComments listener for the dialog
     * @param onUpdateCommentListener
     */
    public void setOnUpdateCommentListener(OnUpdateComments onUpdateCommentListener) {
        mOnUpdateCommentListener = onUpdateCommentListener;
    }


    /**
     * Allows the dialog to detect when it has been flung (quick swipe away)
     */
    public class GestureListener extends android.view.GestureDetector.SimpleOnGestureListener {
        private static final int FLING_THRESHOLD = 2000;
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            // check to see if the velocity is above the threshold
            if (Math.abs(velocityY) > FLING_THRESHOLD) {
                animateDismissDialog();
            }
            return false;
        }
    }

    /**
     * Builder to facilitate the creation of a comment dialog
     */
    public static class Builder {
        private final Bundle mBundle;
        private OnUpdateComments mUpdateCommentListener;

        /**
         * Instantiates the bundle which we store the extra data in
         */
        public Builder() {
            mBundle = new Bundle();
        }

        /**
         * Sets whether the comment input is focused or not when the dialog is first loaded
         * @param focusCommentInput
         * @return
         */
        public Builder setFocusCommentInput(boolean focusCommentInput) {
            mBundle.putBoolean(FOCUS_COMMENT_INPUT_KEY, focusCommentInput);
            return this;
        }

        /**
         * Sets the post that this comment dialog relates to
         * @param post
         * @return
         */
        public Builder setPost(Post post) {
            mBundle.putString(POST_ID, post.getId());
            mBundle.putInt(NUM_LIKES, post.getLikes());
            return this;
        }

        /**
         * Sets the listener which is called when there is new comments that have been loaded
         * from the API or posted by the user. Should be used to update the comment counter
         */
        public Builder setOnUpdateCommentListener(OnUpdateComments updateCommentListener) {
            mUpdateCommentListener = updateCommentListener;
            return this;
        }

        /**
         * Builds the comment dialog, assigns the bundle arguments and returns the instance
         * @return
         */
        public CommentDialogFragment build() {
            CommentDialogFragment commentDialogFragment = new CommentDialogFragment();
            commentDialogFragment.setArguments(mBundle);
            commentDialogFragment.setOnUpdateCommentListener(mUpdateCommentListener);
            return commentDialogFragment;
        }
    }

    /**
     * Interface to notify the view when there are new comments loaded. Used to notify the comment
     * counter on the look book view when there are new comments
     */
    public interface OnUpdateComments {
        void update(int numberOfComments);
    }
}
