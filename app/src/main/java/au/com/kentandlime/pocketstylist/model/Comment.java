package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by paul on 25/09/16.
 * A model for a comment on a post
 */
public class Comment {
    @SerializedName("user")
    private User mUser;

    @SerializedName("comment")
    private String mComment;

    @SerializedName("createdAt")
    private Date mCreatedAt;


    public Comment(User user, String comment, Date createdAt) {
        mUser = user;
        mComment = comment;
        mCreatedAt = createdAt;
    }

    public String getComment() {
        return mComment;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public void setComment(String comment) {
        mComment = comment;
    }
}
