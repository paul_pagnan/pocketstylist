package au.com.kentandlime.pocketstylist.service;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import java.net.URISyntaxException;
import java.util.List;

import au.com.kentandlime.pocketstylist.BuildConfig;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.SocketIoConnectionRequest;
import au.com.kentandlime.pocketstylist.model.User;

/**
 * Service that handles the instant chat functions and connection with the socket io server
 */
public class ChatService {
    private final static String SERVICE_URL = BuildConfig.CHAT_API;
    private final static String ACTION_CONNECT = "connectUser";
    private static final String ACTION_SEND = "sendMessage";
    private static final String ACTION_UPDATE_CHAT = "updateChat";

    private OnMessageReceivedListener mMessageReceivedListener;

    private Socket mSocket;
    private List<ChatMessage> mUnreadPostsFromUser;

    /**
     * Set up the socket io listeners when the app starts for the first time
     */
    public ChatService() {
        try {
            mSocket = IO.socket(SERVICE_URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        setupListeners();
    }

    /**
     * Connect to the socket IO server
     * @param user
     */
    public void connect(User user) {
        mSocket.connect();
        Gson gson = new Gson();
        SocketIoConnectionRequest request = new SocketIoConnectionRequest(user.getEmail(),
                user.getStylist());
        mSocket.emit(ACTION_CONNECT, gson.toJson(request));
    }

    /**
     * Is the socket io connected to the chat server
     * @return
     */
    public boolean isConnected() {
        return mSocket.connected();
    }

    /**
     * Disconnects the current socket io session
     */
    public void disconnect() {
        mSocket.disconnect();
    }

    /**
     * Sets up the listeners for when the socket io server emits an event back to the client
     */
    private void setupListeners() {
        Emitter.Listener onMessageReceived = new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Gson gson = new Gson();
                ChatMessage message = gson.fromJson(args[0].toString(), ChatMessage.class);
                if (mMessageReceivedListener != null) {
                    mMessageReceivedListener.messageReceived(message);
                }
            }
        };
        mSocket.on(ACTION_UPDATE_CHAT, onMessageReceived);
    }

    /**
     * Sends a message to the socket io server
     * @param message
     */
    public void sendMessage(String message) {
        mSocket.emit(ACTION_SEND, message);
    }

    /**
     * Sets the on message received listener for the service
     * @param listener
     */
    public void setOnMessageRecievedListener(OnMessageReceivedListener listener) {
        mMessageReceivedListener = listener;
    }

    /**
     * Gets all messages and marks them as seen
     */
    public void seenAllMessages() {
        List<ChatMessage> chatMessageList = ChatMessage.find(
                ChatMessage.class, "M_SEEN = ?", "0");

        for (ChatMessage chatMessage : chatMessageList) {
            chatMessage.setSeen(true);
            chatMessage.save();
        }
    }

    /**
     * Gets all the messages that the user has not seen yet
     * @return
     */
    public List<ChatMessage> getUnreadPostsFromStylist() {
        String[] whereArgs = { "0", ChatMessage.UserType.STYLIST.toString()};
        return ChatMessage.find(
                ChatMessage.class, "M_SEEN = ? AND M_FROM = ?", whereArgs, null,
                "M_RECEIVED_AT DESC", "5");
    }

    /**
     * Get all the posts that have been sent by the user that the user has not seen yet.
     * This is used when they reply to a message from the inline notification, then navigates to
     * the chat fragment to view the message history
     * @return
     */
    public List<ChatMessage> getAllUnreadMessages() {
        String[] whereArgs = { "0" };
        return ChatMessage.find(
                ChatMessage.class, "M_SEEN = ?", whereArgs,
                null, "M_RECEIVED_AT DESC", null);
    }

    /**
     * Listener used to communicate back to the Presenter when a message has been received
     * from the socket io server
     */
    public interface OnMessageReceivedListener {
        void messageReceived(ChatMessage message);
    }
}
