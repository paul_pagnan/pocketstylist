package au.com.kentandlime.pocketstylist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.service.AuthService;

/**
 * Uses the AppIntro library to show a set of simple swipeable onboarding slides.
 */
public class OnboardingActivity extends AppIntro {
    @Inject
    AuthService mAuthService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInjectorComponent().inject(this);

        // Welcome to pocketstylist
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.welcome),
                getString(R.string.welcome_to_pocket_stylist),
                R.drawable.slide_welcome,
                getResources().getColor(R.color.kalDarkNavy)));


        // Look book
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.look_book),
                getString(R.string.look_book_desription),
                R.drawable.slide_lookbook,
                getResources().getColor(R.color.kalDarkNavy)));

        // Stylist Chat
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.stylist_chat),
                getString(R.string.stylist_chat_description),
                R.drawable.slide_chat,
                getResources().getColor(R.color.kalDarkNavy)));

        // Wardrobe
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.my_wardrobe),
                getString(R.string.my_wardrobe_description),
                R.drawable.slide_wardrobe,
                getResources().getColor(R.color.kalDarkNavy)));

        setFadeAnimation();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finishOnBoarding();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finishOnBoarding();
    }

    /**
     * Calls AuthService to indicate that onboarding is finished and
     * navigates to the main activity
     */
    private void finishOnBoarding() {
        mAuthService.finishOnboarding();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
