package au.com.kentandlime.pocketstylist.activity.inspiration;

import android.content.Context;
import android.widget.Toast;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookFragment;
import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookPresenter;
import au.com.kentandlime.pocketstylist.activity.inspiration.mylooks.MyLooksFragment;
import au.com.kentandlime.pocketstylist.adapter.LookBookAdapter;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.service.ContentService;
import rx.Subscriber;

/**
 * Presenter to provide data to the Inspiration related fragments, Look Book, My Looks and
 * Discover Fragments
 */
public class InspirationPresenter {
    private final Context mContext;
    @Inject
    ContentService mContentService;

    private LookBookAdapter.OnPostLikeListener mLikeListener;
    private MyLooksFragment mMyLooksFragment;
    private LookBookFragment mDiscoverFragment;
    private LookBookFragment mLookBookFragment;

    /**
     * Instantiates the Inspiration presenter and injects dependencies
     * @param context
     */
    public InspirationPresenter(Context context) {
        App.getInjectorComponent().inject(this);
        mContext = context;
    }

    /**
     * Returns the like listener which handles the service calls of liking or unliking a post
     * @return
     */
    public LookBookAdapter.OnPostLikeListener getLikeListener() {
        if (mLikeListener == null) {
            mLikeListener = new LookBookAdapter.OnPostLikeListener() {
                @Override
                public void liked(Post post) {
                    mMyLooksFragment.liked(post);
                    post.incrementLike();
                    mContentService.likePost(post.getId()).subscribe(
                            getGenericSubscriber(R.string.unable_to_like_post, post, true));
                    post.setLiked(true);
                }

                @Override
                public void unLiked(Post post) {
                    mMyLooksFragment.unliked(post);
                    post.decrementLike();
                    mContentService.unlikePost(post.getId()).subscribe(
                            getGenericSubscriber(R.string.unable_to_unlike_post, post, false));
                    post.setLiked(false);
                }
            };
        }
        return mLikeListener;
    }

    /**
     * Returns a generic subscriber that generates a toast message on error
     * @param stringId
     * @return
     */
    private Subscriber<Void> getGenericSubscriber(final int stringId, final Post post,
                                                  final boolean like) {
        return new Subscriber<Void>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (like) {
                    post.decrementLike();
                    mMyLooksFragment.unliked(post);
                    post.setLiked(false);

                } else {
                    post.incrementLike();
                    mMyLooksFragment.liked(post);
                    post.setLiked(true);
                }
                mDiscoverFragment.notifyUpdate();
                mLookBookFragment.notifyUpdate();
                Toast.makeText(mContext, mContext.getString(stringId), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(Void aVoid) {
            }
        };
    }

    /**
     * Attaches the mylooks fragment to the inspiration presenter to allow it to append
     * liked posts ot the my looks view
     * @param myLooksFragment
     */
    public void attachMyLooksFragment(MyLooksFragment myLooksFragment) {
        mMyLooksFragment = myLooksFragment;
    }

    /**
     * Attaches the LookBook fragment to the inspiration presenter to allow it to update the like
     * counter on error
     * @param lookBookFragment
     * @param type
     */
    public void attachLookBookFragment(LookBookFragment lookBookFragment,
                                       LookBookPresenter.LookBookType type) {
        if (type == LookBookPresenter.LookBookType.DISCOVER) {
            mDiscoverFragment = lookBookFragment;
        } else {
            mLookBookFragment = lookBookFragment;
        }
    }
}
