package au.com.kentandlime.pocketstylist.activity.wardrobe;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import au.com.kentandlime.pocketstylist.service.ContentService;
import au.com.kentandlime.pocketstylist.util.DialogUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;

/**
 * Activity to add a item to the user's wardrobe
 * Accepts a bitmap from the previous activity and shows a form to allow the user to tag the item
 */
public class ViewWardrobeActivity extends AppCompatActivity {
    @Inject
    ContentService mContentService;

    @BindView(R.id.add_wardrobe_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.view_wardrobe_brand)
    TextView mBrandTxt;

    @BindView(R.id.view_wardrobe_category)
    TextView mCategoryTxt;

    @BindView(R.id.view_wardrobe_like_reasons)
    TextView mLikeReasonsTxt;

    @BindView(R.id.view_wardrobe_size)
    TextView mSizeTxt;

    @BindView(R.id.view_activity_toolbar_subtitle)
    TextView mToolbarSubtitle;

    @BindView(R.id.view_wardrobe_expanded_image)
    ImageView mHeaderImageView;

    private WardrobeItem mWardrobeItem;

    public static final int RESULT_DELETED = 9;
    private DialogUtil mDialogUtil;
    public static final String KEY_WARDROBE_ITEM = "wardrobeItem";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_view_wardrobe);
        ButterKnife.bind(this);
        App.getInjectorComponent().inject(this);

        mDialogUtil = new DialogUtil(this);
        mWardrobeItem = getIntent().getParcelableExtra(KEY_WARDROBE_ITEM);

        setContent();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(mWardrobeItem.getBrand());
        mToolbarSubtitle.setText(mWardrobeItem.getCategory());
    }

    /**
     * Gets the data from the POJO and populates the view
     */
    private void setContent() {
        mSizeTxt.setText(mWardrobeItem.getSize());
        mBrandTxt.setText(mWardrobeItem.getBrand());
        mCategoryTxt.setText(mWardrobeItem.getCategory());
        mLikeReasonsTxt.setText(TextUtils.join(", ", mWardrobeItem.getLikeReasons()));

        Picasso.with(this).load(mWardrobeItem.getImageUrl()).into(mHeaderImageView);
        mHeaderImageView.setColorFilter(getResources().getColor(R.color.image_overlay),
                PorterDuff.Mode.DARKEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_wardrobe_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.view_wardrone_delete:
                delete();
                break;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Deletes the wardrobe item
     */
    private void delete() {
        final ProgressDialog progressDialog = mDialogUtil.showProgressDialog(R.string.deleting);
        mContentService.deleteWardrobeItem(mWardrobeItem).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {
                progressDialog.dismiss();
            }

            @Override
            public void onError(Throwable e) {
                mDialogUtil.showDismissibleDialog(R.string.error, R.string.error_deleting_wardrobe);
                onCompleted();
            }

            @Override
            public void onNext(Void aVoid) {
                setResult(RESULT_DELETED, getIntent());
                finish();
            }
        });
    }
}
