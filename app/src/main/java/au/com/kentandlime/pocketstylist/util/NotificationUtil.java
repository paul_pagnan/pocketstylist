package au.com.kentandlime.pocketstylist.util;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.NotificationManagerCompat;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Collections;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.MainActivity;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.service.ChatService;

/**
 * Util to assist in creating complex notifications
 */
public class NotificationUtil {
    private static final int INTENT_DELETE_REQUEST_CODE = 1;
    private static final int INTENT_REQUEST_CODE = 0;
    private static final int REPLY_INTENT_REQUEST_CODE = 5;
    private static final int LIGHT_COLOUR = 0xFFAF9570;
    private static final String GROUP_KEY_CHAT = "chat_group";
    public static final String NOTIFICATION_DELETED_ACTION = "notification_cancelled";
    public static final String NOTIFICATION_REPLY_ACTION = "notification_reply";
    public static final int NOTIFICATION_ID = 0;
    public static final String KEY_TEXT_REPLY = "key_text_reply";


    /**
     * Shows the chat notification from the stylist and loads the stylist image from url
     * @param context
     * @param chatService
     * @param newestMessage
     */
    public static void showChatNotifications(final Context context, final ChatService chatService,
                                             final ChatMessage newestMessage, final boolean loud) {
        Picasso.with(context)
            .load(User.getStylistImageUrl(newestMessage.getStylist().toLowerCase()))
            .into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    showChatNotifications(context, chatService, newestMessage, loud, bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Bitmap failedBitmap = BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.emily_bio);
                    showChatNotifications(context, chatService, newestMessage, loud, failedBitmap);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
    }

    /**
     * Shows the chat notification from the stylist
     * @param context
     * @param chatService
     * @param newestMessage
     * @param icon
     */
    public static void showChatNotifications(Context context, ChatService chatService,
                                             ChatMessage newestMessage, boolean loud,
                                             Bitmap icon) {
        Intent deleteIntent = new Intent(NOTIFICATION_DELETED_ACTION);
        PendingIntent deletePendingIntent = PendingIntent.getBroadcast(context,
                INTENT_DELETE_REQUEST_CODE, deleteIntent, 0);

        Intent contentIntent = new Intent(context, MainActivity.class);
        contentIntent.putExtra(MainActivity.KEY_GOTO_CHAT, true);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(context, INTENT_REQUEST_CODE,
                contentIntent, 0);

        List<ChatMessage> messages = chatService.getAllUnreadMessages();

        Notification.Style notificationStyle = Build.VERSION.SDK_INT >= 24 ?
                getMessagingStyle(context, messages) : getInboxStyle(context, messages);
        Notification.Action action = getNotificationAction(context, messages.get(0));

        Notification.Builder summaryNotification = new Notification.Builder(context)
                .setContentTitle(newestMessage.getStylist())
                .setSmallIcon(R.mipmap.notification_icon)
                .setStyle(notificationStyle)
                .setLargeIcon(icon)
                .setGroup(GROUP_KEY_CHAT)
                .setColor(LIGHT_COLOUR)
                .setContentIntent(contentPendingIntent)
                .setDeleteIntent(deletePendingIntent)
                .addAction(action);

        if (loud) {
            summaryNotification
                    .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND
                    | Notification.FLAG_SHOW_LIGHTS)
                    .setLights(LIGHT_COLOUR, 300, 900)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentText(newestMessage.getMessage());
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFICATION_ID, summaryNotification.build());
    }

    /**
     * Gets the notification action for when the user replies to the message
     * inline (Android N feature)
     * @param message
     * @return
     */
    private static Notification.Action getNotificationAction(Context context, ChatMessage message) {
        String replyLabel = context.getResources().getString(R.string.reply);

        RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                .setLabel(replyLabel)
                .build();

        Intent replyIntent = new Intent(NOTIFICATION_REPLY_ACTION);
        PendingIntent replyPendingIntent = PendingIntent.getBroadcast(context,
                REPLY_INTENT_REQUEST_CODE, replyIntent, 0);

        Notification.Action action =
                new Notification.Action.Builder(R.drawable.ic_arrow_back_24dp,
                        replyLabel, replyPendingIntent)
                        .addRemoteInput(remoteInput)
                        .build();
        return action;
    }

    /**
     * Gets the content view of the notification, i.e. the message content to be displayed
     * @param context
     * @param messages
     * @return
     */
    private static Notification.InboxStyle getInboxStyle(Context context,
                                                         List<ChatMessage> messages) {
        ChatMessage message = messages.get(0);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle()
                .setBigContentTitle(message.getStylist());

        if (messages.size() == 5) {
            inboxStyle.addLine("...");
        }

        Collections.reverse(messages);

        for (ChatMessage chatMessage : messages) {
            inboxStyle.addLine(chatMessage.getMessage());
        }
        return inboxStyle;
    }

    /**
     * Gets the content view of the notification in new messaging style of Android N
     * @param context
     * @param messages
     * @return
     */
    @TargetApi(24)
    private static Notification.MessagingStyle getMessagingStyle(Context context,
                                                                 List<ChatMessage> messages) {
        Notification.MessagingStyle messagingStyle = new Notification.MessagingStyle(
                context.getString(R.string.you));

        ChatMessage message = messages.get(0);

        Collections.reverse(messages);

        for (ChatMessage chatMessage : messages) {
            String fromName = chatMessage.getFrom() == ChatMessage.UserType.USER ?
                    null : message.getStylist();
            messagingStyle.addMessage(chatMessage.getMessage(), 0, fromName);
        }
        return messagingStyle;
    }
}
