package au.com.kentandlime.pocketstylist.activity.inspiration.lookbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.activity.RefreshablePaginateableContentView;
import au.com.kentandlime.pocketstylist.activity.inspiration.InspirationPresenter;
import au.com.kentandlime.pocketstylist.adapter.LookBookAdapter;
import au.com.kentandlime.pocketstylist.model.Post;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Displays a list of posts that the user has liked
 */
public class LookBookFragment extends Fragment
        implements RefreshablePaginateableContentView<List<Post>> {
    @Inject
    InspirationPresenter mInspirationPresenter;

    @BindView(R.id.looks_recycler_view)
    RecyclerView mLookRecyclerView;

    @BindView(R.id.looks_empty_view)
    TextView mNoPostsTxt;

    @BindView(R.id.look_refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    private LookBookAdapter mLookBookAdapter;
    private Paginate mPagination;
    private LookBookPresenter.LookBookType mType;
    private LookBookPresenter mLookBookPresenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_look_book, container, false);

        App.getInjectorComponent().inject(this);
        ButterKnife.bind(this, layout);
        mLookBookPresenter = new LookBookPresenter(this, mType);
        mInspirationPresenter.attachLookBookFragment(this, mType);

        mLookBookAdapter = new LookBookAdapter(getContext(), getFragmentManager());
        mLookBookAdapter.setOnLikeListener(mInspirationPresenter.getLikeListener());

        mLookRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLookRecyclerView.setAdapter(mLookBookAdapter);

        mRefreshLayout.setOnRefreshListener(mLookBookPresenter.getRefreshListener());

        // Setup pagination
        mPagination = mLookBookPresenter.getPaginater(mLookRecyclerView);

        return layout;
    }

    @Override
    public void onComplete() {
        if (mLookBookAdapter.getItemCount() == 0) {
            mNoPostsTxt.setVisibility(View.VISIBLE);
        } else {
            mNoPostsTxt.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError() {
        mPagination.unbind();
        Toast.makeText(getContext(), getString(R.string.could_not_refresh),
                Toast.LENGTH_SHORT).show();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemsRetrieved(List<Post> posts) {
        mLookBookAdapter.appendPosts(posts);
        mLookBookAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishRefreshing() {
        mLookBookAdapter.clearAll();
        mRefreshLayout.setRefreshing(false);
    }

    /**
     * Sets the type of data that this fragment will show
     * @param type
     */
    public void setType(LookBookPresenter.LookBookType type) {
        mType = type;
    }

    /**
     * Called from the inspiration presenter when it has modified the like count and needs to
     * update the list view
     */
    public void notifyUpdate() {
        mLookBookAdapter.notifyDataSetChanged();
    }
}
