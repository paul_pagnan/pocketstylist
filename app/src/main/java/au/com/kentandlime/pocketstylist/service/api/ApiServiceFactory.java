package au.com.kentandlime.pocketstylist.service.api;

import retrofit.RestAdapter;

/**
 * Factory to create a Retrofit Service for each API service.
 */
public class ApiServiceFactory {
    /**
     * Generic typed method to create a retrofit service based on the passed in ApiInterface
     * @param clazz
     * @param endPoint
     * @return
     */
    public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint) {
        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        T service = restAdapter.create(clazz);

        return service;
    }
}
