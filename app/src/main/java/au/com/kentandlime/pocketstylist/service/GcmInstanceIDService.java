package au.com.kentandlime.pocketstylist.service;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Service to listen for GCM token changes and pass off to RegistrationService to handle
 */
public class GcmInstanceIDService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes
        Intent intent = new Intent(this, GcmRegistrationService.class);
        startService(intent);
    }
}
