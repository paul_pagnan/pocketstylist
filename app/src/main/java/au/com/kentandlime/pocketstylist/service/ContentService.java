package au.com.kentandlime.pocketstylist.service;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.activity.inspiration.lookbook.LookBookPresenter;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.Comment;
import au.com.kentandlime.pocketstylist.model.PaginatedResponse;
import au.com.kentandlime.pocketstylist.model.Post;
import au.com.kentandlime.pocketstylist.model.User;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import au.com.kentandlime.pocketstylist.service.api.ContentApiInterface;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Service to orchestrate the connection and caching between client
 * and Content API
 */
public class ContentService extends BaseService<ContentApiInterface> {

    /**
     * Creates a new content service and calls the constructor of the bast service
     * @param context
     */
    @Inject
    public ContentService(Context context) {
        super(context, ContentApiInterface.SERVICE_ENDPOINT, ContentApiInterface.class);
    }

    /**
     * Registers the user to the content API by simply passing the JWT in the auth header
     * @return
     */
    public Observable<User> register() {
        return mService.register()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets a paginated list of posts from the server. Page size is 10
     * Accepts a type, if DISCOVER it will get the posts that do not match the user's style profile
     * if LOOK_BOOK, it will get posts that match the user's style
     * @param page
     * @param lookBookType
     * @return
     */
    public Observable<PaginatedResponse<List<Post>>> getPosts(
            int page, LookBookPresenter.LookBookType lookBookType) {
        if (lookBookType == LookBookPresenter.LookBookType.DISCOVER) {
            return mService.getDiscoverPosts(page)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        }

        return mService.getLookBookPosts(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets a paginated list of the posts that a user has liked. Page size is 20
     * @param page
     * @return
     */
    public Observable<PaginatedResponse<List<Post>>> getMyLooks(int page) {
        return mService.getMyLooks(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Likes a single post
     * @param id the id of the post
     * @return
     */
    public Observable<Void> likePost(String id) {
        return mService.likePost(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Unlikes a single post
     * @param id the id of the post
     * @return
     */
    public Observable<Void> unlikePost(String id) {
        return mService.unlikePost(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets all the comments for a particular post
     * @param id the id of the post
     * @return
     */
    public Observable<List<Comment>> getComments(String id) {
        return mService.getComments(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets all the comments for a particular post
     * @param id the id of the post
     * @return
     */
    public Observable<Void> postComment(String id, String comment) {
        return mService.postComment(id, comment)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets a list of chat messages from the server by page. Page size is 15.
     * @param page
     * @return
     */
    public Observable<PaginatedResponse<List<ChatMessage>>> getMessages(int page) {
        return mService.getMessages(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    /**
     * Sends the GCM registration token to the server
     * @param token
     * @param user
     */
    public Observable<Void> registerGcmToken(String token, User user) {
        return mService.registerGcmToken(user.getId(), token)
                .observeOn(Schedulers.newThread());
    }


    /**
     * Adds a wardrobe item
     * @param wardrobeItem
     */
    public Observable<WardrobeItem> addWardrobeItem(WardrobeItem wardrobeItem) {
        return mService.addWardrobeItem(getJson(wardrobeItem))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Gets a paginated list of wardrobe items by the current user
     * @param page
     */
    public Observable<PaginatedResponse<List<WardrobeItem>>> getWardrobeItems(int page) {
        return mService.getWardrobeItems(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Updates a single wardrobe item
     * @param wardrobeItem
     */
    public Observable<Void> updateWardrobeItem(WardrobeItem wardrobeItem) {
        return mService.updateWardrobeItem(wardrobeItem.getId(), getJson(wardrobeItem))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Deletes a single wardrobe item
     * @param wardrobeItem
     */
    public Observable<Void> deleteWardrobeItem(WardrobeItem wardrobeItem) {
        return mService.deleteWardrobeItem(wardrobeItem.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
