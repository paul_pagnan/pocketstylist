package au.com.kentandlime.pocketstylist.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter to display the chat items in the stylist chat view
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private final List<ChatMessage> mChatMessages;
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final User mUser;

    public ChatAdapter(Context context, User user) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mChatMessages = new ArrayList<>();
        mUser = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_chat_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatMessage chatMessage = mChatMessages.get(position);
        boolean isFromUser = chatMessage.getFrom() == ChatMessage.UserType.USER;

        //Setup layout
        int backgroundResId = isFromUser ? R.drawable.message_user : R.drawable.message_stylist;
        Drawable bgDrawable = ResourcesCompat.getDrawable(mContext.getResources(),
                backgroundResId, null);

        // set high level layout parameters such as background color and alignment
        int gravity = isFromUser ? Gravity.END : Gravity.START;

        int textColorResId = isFromUser ? R.color.white : R.color.kalTextDark;
        int textColor = ResourcesCompat.getColor(mContext.getResources(), textColorResId, null);

        // set a gap between blocks of messages
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)
                holder.parent.getLayoutParams();
        params.bottomMargin = 0;

        if (position > 0) {
            if (!mChatMessages.get(position - 1).getFrom().equals(chatMessage.getFrom())) {
                params.bottomMargin = (int) mContext.getResources()
                        .getDimension(R.dimen.chat_message_gap);
            }
        }

        // set to the views
        holder.parent.setLayoutParams(params);
        holder.chatMessage.setBackground(bgDrawable);
        holder.parent.setGravity(gravity);
        holder.chatMessage.setTextColor(textColor);

        setupImageAndNameView(holder, position, chatMessage);

        //Set data
        holder.chatMessage.setText(chatMessage.getMessage());
        holder.stylistName.setText(mUser.getStylist());

        Picasso.with(mContext).load(mUser.getStylistImageUrl()).into(holder.stylistImage);
    }

    /**
     * Defines the logic that determins whether or not to show the senders name or profile image
     * for each recycler list item
     * @param holder
     * @param position
     * @param chatMessage
     */
    private void setupImageAndNameView(ViewHolder holder, int position, ChatMessage chatMessage) {
        // setup defaults
        holder.stylistContainer.setVisibility(View.VISIBLE);
        holder.stylistImage.setVisibility(View.GONE);
        holder.stylistName.setVisibility(View.GONE);

        if (chatMessage.getFrom() != ChatMessage.UserType.STYLIST) {
            // message is from user. Hide the image container and do nothing
            holder.stylistContainer.setVisibility(View.GONE);
            return;
        }

        if (position > 0) {
            if (mChatMessages.get(position - 1).getFrom().equals(ChatMessage.UserType.USER)) {
                // last item in stylist message block - show their image
                holder.stylistImage.setVisibility(View.VISIBLE);
            }
        } else {
            // last message in the list is by the stylist - show image
            holder.stylistImage.setVisibility(View.VISIBLE);
        }

        if (position < getItemCount() - 1) {
            if (mChatMessages.get(position + 1).getFrom().equals(ChatMessage.UserType.USER)) {
                // first stylist message in block - show name
                holder.stylistName.setVisibility(View.VISIBLE);
            }
        }
        else {
            // very first message sent - show stylist name and image
            holder.stylistImage.setVisibility(View.VISIBLE);
            holder.stylistName.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mChatMessages.size();
    }

    /**
     * Appends a collection of messages to the list
     * @param data
     */
    public void appendMessages(List<ChatMessage> data) {
        mChatMessages.addAll(data);
    }

    /**
     * Prepends a collection of messages to the list
     * @param data
     */
    public void prependMessages(List<ChatMessage> data) {
        mChatMessages.addAll(0, data);
    }

    /**
     * Adds a new chat message to the start of the list
     * @param chatMessage
     */
    public void addMessage(ChatMessage chatMessage) {
        mChatMessages.add(0, chatMessage);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.chat_message)
        TextView chatMessage;

        @BindView(R.id.chat_stylist_name_txt)
        TextView stylistName;

        @BindView(R.id.chat_stylist_image_view)
        ImageView stylistImage;

        @BindView(R.id.chat_stylist_image_container)
        LinearLayout stylistContainer;

        @BindView(R.id.chat_message_container)
        LinearLayout parent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
