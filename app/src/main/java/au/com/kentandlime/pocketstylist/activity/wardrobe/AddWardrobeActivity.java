package au.com.kentandlime.pocketstylist.activity.wardrobe;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import au.com.kentandlime.pocketstylist.R;
import au.com.kentandlime.pocketstylist.model.WardrobeItem;
import au.com.kentandlime.pocketstylist.service.WardrobeUploadService;
import au.com.kentandlime.pocketstylist.util.DialogUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity to add a item to the user's wardrobe
 * Accepts a bitmap from the previous activity and shows a form to allow the user to tag the item
 */
public class AddWardrobeActivity extends AppCompatActivity {
    public static final String KEY_WARDROBE_IMAGE = "wardrobeImage";
    public static final String KEY_IS_CONTENT_URI = "isContentUri";
    public static final String KEY_WARDROBE_ITEM = "wardrobeItem";

    @BindView(R.id.add_wardrobe_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.add_wardrobe_expanded_image)
    ImageView mHeaderImage;

    @BindView(R.id.add_wardrobe_brand_et)
    EditText mBrandEt;

    @BindView(R.id.add_wardrobe_size_et)
    EditText mSizeEt;

    @BindView(R.id.add_wardrobe_category_spinner)
    Spinner mCategorySpinner;

    private List<String> mSelectedTags = new ArrayList<>();
    private Uri mImageUri;
    private boolean mIsContentUri;
    private DialogUtil mDialogUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wardrobe);
        ButterKnife.bind(this);

        mDialogUtil = new DialogUtil(this);

        mImageUri = getIntent().getParcelableExtra(KEY_WARDROBE_IMAGE);
        mIsContentUri = getIntent().getBooleanExtra(KEY_IS_CONTENT_URI, false);

        mToolbar.setTitle(getString(R.string.add_to_wardrobe));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mHeaderImage.setImageURI(mImageUri);
        mHeaderImage.setColorFilter(getResources().getColor(R.color.image_overlay),
                PorterDuff.Mode.DARKEN);

        String[] arraySpinner = getResources().getStringArray(R.array.wardrobe_categories);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        mCategorySpinner.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_wardrobe_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.add_wardrone_save:
                if (formValid()) {
                    // Start the upload service
                    Intent intent = new Intent(this, WardrobeUploadService.class);
                    intent.putExtra(KEY_WARDROBE_IMAGE, mImageUri);
                    intent.putExtra(KEY_IS_CONTENT_URI, mIsContentUri);
                    intent.putExtra(KEY_WARDROBE_ITEM, createWardrobeItem());
                    startService(intent);

                    setResult(Activity.RESULT_OK);
                    finish();
                    return true;
                }
                break;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Validates the user input and returns if the form is valid or not. If not valid this
     * method will show a warning dialog to the user.
     * @return
     */
    private boolean formValid() {
        String brand = mBrandEt.getText().toString();
        String size = mSizeEt.getText().toString();
        if (brand.isEmpty() || size.isEmpty() || mSelectedTags.size() == 0) {
            mDialogUtil.showDismissibleDialog(
                    R.string.vaidation_error,
                    R.string.complete_all_fields);
            return false;
        }
        return true;
    }

    /**
     * Called from the view when a tag is selected
     * @param view
     */
    public void selectTag(View view) {
        Button button = (Button) view;
        String tagText = button.getText().toString();

        if (mSelectedTags.contains(tagText)) {
            mSelectedTags.remove(tagText);
            button.setBackgroundTintList(getResources().getColorStateList(R.color.kalGrey));
            button.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            mSelectedTags.add(tagText);
            button.setBackgroundTintList(getResources().getColorStateList(R.color.kalBeige));
            button.setTextColor(getResources().getColor(R.color.white));
        }
    }

    /**
     * Creates a WardrobeItem model from the data contained in the form
     * @return
     */
    public WardrobeItem createWardrobeItem() {
        return new WardrobeItem(
                null, mBrandEt.getText().toString(),
                mSizeEt.getText().toString(),
                mCategorySpinner.getSelectedItem().toString(),
                null, null, mSelectedTags);
    }
}
