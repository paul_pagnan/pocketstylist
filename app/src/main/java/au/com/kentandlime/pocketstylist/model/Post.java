package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * POJO for a social post tobe displayed on the main screen list view
 */
public class Post {
    @SerializedName("createdAt")
    private Date mCreatedAt;

    @SerializedName("image")
    private String mImageUrl;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("stylist")
    private String mStylist;

    @SerializedName("likes")
    private int mLikes;

    @SerializedName("comments")
    private int mComments;

    @SerializedName("_id")
    private String mId;

    @SerializedName("liked")
    private boolean mLiked;

    public Post(Date createdAt, String imageUrl, String description, String stylist, int likes,
                int comments, String id, boolean liked) {
        mCreatedAt = createdAt;
        mImageUrl = imageUrl;
        mDescription = description;
        mStylist = stylist;
        mLikes = likes;
        mComments = comments;
        mId = id;
        mLiked = liked;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getStylist() {
        return mStylist;
    }

    public void setStylist(String stylist) {
        mStylist = stylist;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int likes) {
        mLikes = likes;
    }

    public int getComments() {
        return mComments;
    }

    public void setComments(int comments) {
        mComments = comments;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public boolean isLiked() {
        return mLiked;
    }

    public void setLiked(boolean liked) {
        mLiked = liked;
    }

    /**
     * Increment the number of likes on the post
     */
    public void incrementLike() {
        mLikes++;
    }

    /**
     * Decrement the number of likes on the post
     */
    public void decrementLike() {
        mLikes--;
    }
}
