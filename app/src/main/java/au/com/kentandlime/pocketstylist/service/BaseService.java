package au.com.kentandlime.pocketstylist.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 *
 * Base service class that creates a Retrofit service instance and appends headers
 */
public class BaseService<T> {
    private static final String AUTHORIZATION_KEY = "Authorization";
    private static final String BEARER_KEY = "Bearer";
    private static final String JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    protected T mService;

    /**
     * Instantiates the retrofit service and appends necessary authentication headers to each
     * request sent through it
     * @param context
     * @param serviceEndpoint
     * @param apiClass
     */
    protected BaseService(Context context, String serviceEndpoint, Class<T> apiClass) {
        final SharedPreferences prefs = context.getSharedPreferences(
                AuthService.AUTH_SHARED_PREFS_KEY,
                Context.MODE_PRIVATE);

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                String jwt = prefs.getString(AuthService.JWT_SHARED_PREFS_KEY, "");
                request.addHeader(AUTHORIZATION_KEY, BEARER_KEY.concat(" ").concat(jwt));
            }
        };

        Gson gson = new GsonBuilder()
                .setDateFormat(JSON_DATE_FORMAT)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(serviceEndpoint)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .build();

        mService = restAdapter.create(apiClass);
    }

    /**
     * Converts an object to JSON using the Gson library
     * @param object
     * @return
     */
    protected String getJson(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}
