package au.com.kentandlime.pocketstylist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model to define the data from the User Api request
 */
public class User {
    private static final String STYLIST_IMAGE_URL =
            "https://hub.kentandlime.com.au/assets/img/{name}_bio.png";
    public static final String DEFAULT_STYLIST = "Denice";

    @SerializedName("id")
    private String mId;

    @SerializedName("firstName")
    private String mFirstName;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("stylist")
    private String mStylist;

    public User(String firstName, String email, String stylist, String id) {
        mFirstName = firstName;
        mEmail = email;
        mStylist = stylist;
        mId = id;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setStylist(String mStylist) {
        this.mStylist = mStylist;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    /**
     * Returns the stylist with the first letter capitalised
     * @return
     */
    public String getStylist() {
        if (mStylist == null) mStylist = DEFAULT_STYLIST;
        return mStylist.substring(0, 1).toUpperCase() + mStylist.substring(1);
    }

    /**
     * Gets image URL for the stylist of the current user
     * @return
     */
    public String getStylistImageUrl() {
        return getStylistImageUrl(mStylist);
    }
    /**
     * Gets image URL for a stylist
     * @return
     */
    public static String getStylistImageUrl(String stylist) {
        return STYLIST_IMAGE_URL.replace("{name}", stylist.toLowerCase());
    }
}
