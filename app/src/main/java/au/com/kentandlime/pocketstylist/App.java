package au.com.kentandlime.pocketstylist;

import android.app.Application;

import com.orm.SugarContext;

import au.com.kentandlime.pocketstylist.dagger.AppModule;
import au.com.kentandlime.pocketstylist.dagger.DaggerInjectorComponent;
import au.com.kentandlime.pocketstylist.dagger.InjectorComponent;
import au.com.kentandlime.pocketstylist.dagger.InjectorModule;

/**
 * App wrapper to inject the AppModule container into the Dagger Components
 */
public class App extends Application {

    private static InjectorComponent mInjectorComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        SugarContext.init(this);

        mInjectorComponent = DaggerInjectorComponent.builder()
                .appModule(new AppModule(this))
                .injectorModule(new InjectorModule())
                .build();
    }

    public static InjectorComponent getInjectorComponent() {
            return mInjectorComponent;
    }
}

