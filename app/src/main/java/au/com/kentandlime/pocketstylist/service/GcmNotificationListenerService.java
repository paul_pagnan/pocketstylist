package au.com.kentandlime.pocketstylist.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;

import javax.inject.Inject;

import au.com.kentandlime.pocketstylist.App;
import au.com.kentandlime.pocketstylist.model.ChatMessage;
import au.com.kentandlime.pocketstylist.util.NotificationUtil;

/**
 * Service to listen for GCM downstream notifications and act upon them
 */
public class GcmNotificationListenerService extends GcmListenerService {
    @Inject
    ChatService mChatService;

    @Inject
    AuthService mAuthService;

    private static final String DATA_KEY = "data";

    public GcmNotificationListenerService() {
        App.getInjectorComponent().inject(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String messageJson = data.getString(DATA_KEY);

        if (!mAuthService.isLoggedIn()) return;

        Gson gson = new Gson();
        final ChatMessage message = gson.fromJson(messageJson, ChatMessage.class);

        // Insert into DB
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                message.setSeen(false);
                message.save();
                NotificationUtil.showChatNotifications(GcmNotificationListenerService.this,
                        mChatService, message, true);
            }
        });
    }
}
